X11-Basic for Android
=====================

(c) 1991-2022 by Markus Hoffmann

<img alt="Logo" src="fastlane/metadata/android/en-US/images/icon.png" width="120" />

X11-Basic is a dialect of the BASIC programming language with graphics
capability. It has a very rich command set, though it is still easy to learn.
The syntax is most similar to GFA-Basic for ATARI-ST. It is a structured dialect
with no line numbers. A full manual and command reference is available.

GFA-programs should run with only a few changes. Also DOS/QBASIC programmers
will feel comfortable.

This implementation is one of the fastest basic interpreters for Android.
Programs can be compiled into a platform independent bytecode.

You can directly type in commands and formulas, which are being evaluated. This
way the interpreter works also as a pocket calculator. It supports complex
numbers and big integers.

Launcher shortcuts can be placed on the desktop to directly execute BASIC
programs.

Basic programs can be written with any (third party) text editor.

The BASIC programs must be placed into the bas folder (/mnt/sdcard/bas). 

Many more example programs can be found in this collection:
https://codeberg.org/kollo/X11-Basic_examples

For further documentation please refer to the X11-Basic home page:
http://x11-basic.sourceforge.net/

The WRITE EXTERNAL STORAGE permission is needed to write to the file system, The
COARSE and FINE LOCATION permissions are needed to get the location with the GPS
commands. The INTERNET permission is needed for all of the internet i/o
functions to work.


X11-Basic für Android
======================

(c) 1991-2022 by Markus Hoffmann

Ein BASIC Interpreter und Compiler


X11-BASIC ist ein Dialekt der Programmiersprache BASIC.

Der Dialekt ist stark an GFA-Basic (für den ATARI ST) angelehnt. Es verfügt über
einen reichen Befehlssatz, viele Grafik-Kommandos und auch spezielle Kommandos
zum Ausnutzen der Besonderheiten der Smartphones und Tablet-Computer. Die
Sprache ist dennoch leicht zu erlernen. Ein komplettes Handbuch steht zur
Verfügung (leider bisher nur auf Englisch).

GFA-Programme sollen mit nur wenigen Änderungen ausgeführt werden können. Auch
DOS/QBASIC Programmierer werden sich wohl fühlen.

Diese Implementierung ist eine der schnellsten Interpreter der
Programmiersprache BASIC für Android.

Der Interpreter ist mit einer kompletten VT100/ANSI Terminalemulation
ausgestattet. Außerdem mit einem 16-Kanal ADSR-Sound-Synthesizer.

Sie können im Direktmodus Befehle und Formeln eingeben, die sofort ausgewertet
werden. So haben Sie auch gleich einen Taschenrechner.

Basic-Programme können mit jedem Text-Editor (z.B. Ted, Jota) geschrieben werden.

Die BASIC-Programme müssen in den bas-Ordner (/mnt/sdcard/bas) platziert werden.
Einige Beispielprogramme sind mit dabei. 

Viele weitere Beispielprogramme gibt es in dieser Sammlung: 
https://codeberg.org/kollo/X11-Basic_examples

Eine vollständige Dokumentation aller Kommandos und Funktionen findet sich im
Benutzerhandbuch und weiteren Dokumenten auf der X11-Basic Homepage:

http://x11-basic.sourceforge.net/

Die WRITE EXTERNAL STORAGE Erlaubnis ist erforderlich, um auf das Dateisystem
schreiben zu können, Die LOCATION Erlaubnisse sind nötig, um den Standort mit
den GPS-Befehlen lesen zu können. Die INTERNET Erlaubnis ist nötig, damit die
Internet-Kommandos funktionieren.

### Download

[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/app/net.sourceforge.x11basic)

### Screenshots

<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width="30%">
</div>

### Important Note:

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.



Acknowledgements
================

Thanks to all people, who helped me to realize this package.

Many  thanks  to  the developers of GFA-Basic. This basic made me
start programming in the 1980s. Many ideas and most of  the  command 
syntax has been taken from the ATARI ST implementation.

Thanks to sourceforge.net and codeberg.org for hosting this project on the web.

I would like to thank every people who help me out with source code, 
patches, program examples, bug tracking, help and documentation writing, 
financial support, judicious remarks, and so on...

And here thanks to people, who helped me recently:

in 2012:
* Marcos Cruz (beta testing and bug fixing)
* Bernhard Rosenkraenzer (send me a patch for 64bit version)

in 2013:
* Matthias Vogl (\verb|va_copy| patch for 64bit version)
* Eckhard Kruse (for permission to include ballerburg.bas in the samples)
* Stewart C. Russell (helped me fix some compilation bugs on Raspberry PI)
* Marcos Cruz (beta testing and bug fixing)
* James V. Fields (beta testing and correcting the manual)

in 2014:
* John Clemens, Slawomir Donocik, Izidor Cicnapuz, Christian Amler,
  Marcos Cruz, Charles Rayer, Bill Hoyt, and John Sheales (beta testing and 
  bug fixing),
  Nic Warne and Duncan Roe for helpful patches for the linux target.

in 2015:
* Guillaume Tello, Wanderer, and John Sheales  (beta testing and bug fixing)

in 2016 and 2017:
* John Sheales  (beta testing and bug fixing)
* bruno xxx (helping with the compilation tutorial for Android)
* David Hall  (bug fixing)
* Emil Schweikerdt  (bug fixing)

in 2018 and 2019:
* Alan (beta testing, bugfix)
* John Sheales  (beta testing and bug fixing)
* Yet Another Troll  (beta testing and bug fixing)

in 2020:
* amaendle (bug fixing)
* John Sheales  (beta testing and bug fixing)


X11-Basic is build on top of many free softwares, and could not exist without 
them.

X11-Basic uses the fast discrete Fourier and cosine transforms and inverses 
by Monty <xiphmont@mit.edu> released to the public domain from 
THE OggSQUISH SOFTWARE CODEC.

X11-Basic uses functionallity of the gmp library, 
the GNU multiple precision arithmetic library, Version 5.1.3.
   Copyright 1991, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
2001, 2002, 2003, 2004, 2005, 2006, 2007 Free Software Foundation, Inc.

For details, see: https://gmplib.org/

X11-Basic also uses functionallity if the LAPACK library. 
LAPACK (Linear Algebra Package) is a standard software library for 
numerical linear algebra. 

For details, see: http://www.netlib.org/lapack/

X11-Basic uses the lodepng code for the PNG bitmap graphics support.
Copyright (c) 2005-2013 Lode Vandevenne

X11-Basic uses a md5 algorithm  Copyright (c) 2001 by Alexander Peslyak (public domain).
X11-Basic uses a sha1 algorithm Copyright (C) 2001-2003  Christophe Devine (GPLv2+)


Further notes
=============

The sources for libx11basic and the X11-Basic user manual are maintained in 
the X11-Basic main repository, called "X11Basic" and hosted on codeberg.org. 
They are just cloned into this repository.


Building the .apk file
======================

The build uses the gradle environment. A ./gradlew build should do.

X11-Basic needs libx11basic.so (including clapack) and libgmp.so. 

libx11basic.so will completely be build from sources (including clapack).

libgmp.so currently relies on an external git repository which offers 
prebuild libraries for android (which have been build from the 
original GMP sources). The build scrips are there but require a 64-bit linux
environment. So far, I haven't managed to built these libraries myself, so a 
little trust in that external project is still required.

Remember cloning this repository with the --recursive option, because it has
submodules (libgmp):

git clone --recursive git@codeberg.org:kollo/X11-Basic.git

then do a 
cd X11-Basic
./gradlew build
(Enter passwords for the keystore)
(the compile process will take a while, clapack really has many files)

The apk should finally be in build/outputs/apk/X11-Basic-release.apk

A more detailed compilation tutorial is now included in doc/.

