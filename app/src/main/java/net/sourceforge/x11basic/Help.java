package net.sourceforge.x11basic;

/* Help.java (c) 2011-2015 by Markus Hoffmann
 *
 * This file is part of X11-Basic for Android, (c) by Markus Hoffmann 1991-2015
 * ============================================================================
 * X11-Basic for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 
 

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;

import android.content.res.AssetManager;
import android.util.Log;


public class Help {
	private final static String TAG="Help";

	/*Get something useful in html from  manual in asset folder */
	
	public static String getonlinehelp(AssetManager assetManager, String key) {
		String t="sorry, nothing found.";
		key=key.toUpperCase(Locale.US);
		String[] files = null; 
		try {
			files = assetManager.list("manual");
		} catch (IOException e) {return("ERROR:"+e.toString());}

		Log.d(TAG,key);
		for(String filename : files) {
			if(filename.charAt(0)==key.charAt(0) && filename.endsWith(".sec")) {
				InputStream in = null;
				try {
					in = assetManager.open("manual"+"/"+filename);
					BufferedReader reader = new BufferedReader(new InputStreamReader(in));
					String line, results = "";
					boolean flag=false,pre=false;
					while( ( line = reader.readLine() ) != null) {
						line=line.replace("&","&amp;");
						line=line.replace("\"","&quot;");
						line=line.replace("\\$","$");
						line=line.replace("<","&lt;");
						line=line.replace(">","&gt;");
						if((line.startsWith("Command:") && line.contains(key)) ||
								(line.startsWith("Function:") && line.contains(key)) ||
								(line.startsWith("Operator:") && line.contains(key)) ||
								(line.startsWith("Variable:") && line.contains(key))
								) flag=true;
						if(flag && (line.startsWith("EXAMPLE:") || line.startsWith("EXAMPLES:") )) {
							pre=true;
							results += line+"\n<pre>";
						} else 
							if(flag && line.startsWith("#####")) {
								flag=false;
								if(pre) results += "</pre>\n";
								pre=false;
								results += "<hr>"+"\n";
							}
							else if(flag) results += line+"\n";
					}
					reader.close();
					if(pre) results += "</pre>\n";
					in.close();
					in = null;
					t=results;
				} catch(Exception e) {
					Log.e(TAG, e.toString());
				}
			}
		}
		t=t.replace("\n", "<br>");
		// t=t.replace("%", "%%");
		t=t.replace("\\hline", "<hr>");
		return t;
	}

	/* Get release notes from file in assets and convert it to html*/

	public static String getreleasenotes(AssetManager assetManager) {
		String t="sorry, nothing found.";
		final String filename="RELEASE_NOTES";
				InputStream in = null;
				try {
					in = assetManager.open("manual"+"/"+filename);
					BufferedReader reader = new BufferedReader(new InputStreamReader(in));
					String line, results = "";
					while( ( line = reader.readLine() ) != null) {
						line=line.replace("&","&amp;");
						line=line.replace("\"","&quot;");
						line=line.replace("\\$","$");
						line=line.replace("<","&lt;");
						line=line.replace(">","&gt;");
						results += line+"\n";
					}
					reader.close();
					in.close();
					in = null;
					t=results;
				} catch(Exception e) {
					Log.e(TAG, e.toString());
				}
		t=t.replace("\n", "<br>");
		// t=t.replace("%", "%%");
		t=t.replace("\\hline", "<hr>");
		return t;
	}
}
