package net.sourceforge.x11basic;

/* PreferencesActivity.java (c) 2011-2015 by Markus Hoffmann
 *
 * This file is part of X11-Basic for Android, (c) by Markus Hoffmann 1991-2015
 * ============================================================================
 * X11-Basic for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;

public class PreferencesActivity extends PreferenceActivity  implements OnSharedPreferenceChangeListener {
	private final static String HOMEPAGE="http://x11-basic.sourceforge.net/";
	private final static String WIKI_PAGE="https://sourceforge.net/p/x11-basic/discussion/";
	private final static String GUESTBOOK_PAGE="https://sourceforge.net/projects/x11-basic/reviews/";
	private final static String EXAMPLES_PAGE="https://codeberg.org/kollo/X11-Basic_examples";
	private final static String MANUAL_PAGE="http://x11-basic.sourceforge.net/X11-Basic-manual.pdf";
	private final static String LICENSE_PAGE="http://x11-basic.sourceforge.net/COPYING";
	private final static String SOURCE_PAGE="https://codeberg.org/kollo/X11-Basic";
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

		findPreference("about_version").setSummary(applicationVersion());
             
		//    findPreference("about_license").setSummary(Constants.LICENSE_URL);
		//    findPreference("about_source").setSummary(Constants.SOURCE_URL);
		findPreference("about_homepage").setSummary(HOMEPAGE);
		findPreference("about_guestbook").setSummary(GUESTBOOK_PAGE);
		findPreference("about_wiki").setSummary(WIKI_PAGE);
		findPreference("about_example_programs").setSummary(EXAMPLES_PAGE);
		findPreference("about_manual").setSummary(MANUAL_PAGE);
		//     findPreference("about_market_app").setSummary(String.format("market://details?id=%s", getPackageName()));
		//     findPreference("about_market_publisher").setSummary("market://search?q=pub:\"Markus Hoffmann\"");
		for(int i=0;i<getPreferenceScreen().getPreferenceCount();i++){
			initSummary(getPreferenceScreen().getPreference(i));
		}
	}
	@Override
	protected void onResume() {
		super.onResume();
		// Set up a listener whenever a key changes             
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}
	@Override
	protected void onPause() {
		super.onPause();
		// Unregister the listener whenever a key changes             
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);     
	}
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) { 
		updatePrefSummary(findPreference(key));
		X11BasicActivity.setfontsize((int)(Double.parseDouble(sharedPreferences.getString("prefs_fontsize", "0"))));
		X11BasicActivity.setfocus((int)(Double.parseDouble(sharedPreferences.getString("prefs_focus", "3"))));
	}
	private void initSummary(Preference p){
		if (p instanceof PreferenceCategory){
			PreferenceCategory pCat = (PreferenceCategory)p;
			for(int i=0;i<pCat.getPreferenceCount();i++) initSummary(pCat.getPreference(i));
		} else updatePrefSummary(p);
	}
	private void updatePrefSummary(Preference p){
		if (p instanceof ListPreference) {
			ListPreference listPref = (ListPreference) p; 
			p.setSummary(listPref.getEntry()); 
		}
		if (p instanceof EditTextPreference) {
			EditTextPreference editTextPref = (EditTextPreference) p; 
			p.setSummary(editTextPref.getText()); 
		}
	}
	private String applicationVersion() {
		try {return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;}
		catch (NameNotFoundException x)  {return "unknown";}
	}
	@Override
	public boolean onPreferenceTreeClick(final PreferenceScreen preferenceScreen, final Preference preference)  {
		final String key = preference.getKey();
		if ("about_license".equals(key)) {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LICENSE_PAGE)));
			finish();
		} else if ("about_version".equals(key)) {
        		startActivity(new Intent(PreferencesActivity.this, AboutActivity.class));
        		finish();
        	} else if ("about_example_programs".equals(key)) {
        		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(EXAMPLES_PAGE)));
        		finish();
        	} else if ("about_source".equals(key)) {
        		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SOURCE_PAGE)));
        		finish();
        	} else if ("about_homepage".equals(key)) {
        		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(HOMEPAGE)));
        		finish();
        	} else if ("about_guestbook".equals(key)) {
        		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(GUESTBOOK_PAGE)));
        		finish();       	
        	} else if ("about_wiki".equals(key)) {
        		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(WIKI_PAGE)));
        		finish();
        	} else if ("about_manual".equals(key)) {
        		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MANUAL_PAGE)));
        		finish();
        	} else if ("about_credits_ttconsole".equals(key)) {
        		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://opentom.org/TomTom_Console")));
        		finish();
        	} else if ("about_market_app".equals(key)) {
        		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("market://details?id=%s", getPackageName()))));
        		finish();
        	} else if ("about_market_publisher".equals(key))  {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:\"Markus Hoffmann\"")));
        		finish();
        	}
		return false;
	}
}
