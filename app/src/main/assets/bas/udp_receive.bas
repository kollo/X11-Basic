'
' This example program listens on port 5555 for UDP packages
' and prints out what it gets.
'
' See also udp_send.bas for how to send messages.
'                                     (c) markus hoffmann 2005
'

' You can run many different independant receivers on different ports.
' Port numbers > 1000 can be used
'
port=5555

OPEN "UU",#1,"listener",port

lastadr=0

DO
  t$=@getmessage$()
  IF LEN(t$)
    a=CVI(LEFT$(t$,2))
    PRINT "received #";a;" from "+@decode_adr$(lastadr)+": ";RIGHT$(t$,LEN(t$)-2)
  ENDIF
LOOP
CLOSE #1
QUIT

FUNCTION decode_adr$(a)
  LOCAL b$
  b$=MKL$(a)
  RETURN STR$(ASC(LEFT$(b$)))+"."+STR$(ASC(MID$(b$,2,1)))+"."+STR$(ASC(MID$(b$,3,1)))+"."+STR$(ASC(RIGHT$(b$)))
ENDFUNCTION

PROCEDURE sendmessage(id,m$,adr)
  LOCAL s$
  s$=MKI$(id)+m$
  SEND #1,s$,adr,port+1
RETURN
PROCEDURE sendACK(pid,adr)
  @sendmessage(6,CHR$(pid),adr)
RETURN
PROCEDURE sendnack(adr)
  @sendmessage(21,"",adr)
RETURN
FUNCTION getmessage$()
  LOCAL t$,adr,pid
  RECEIVE #1,t$,adr
  pid=CVI(MID$(t$,1,2))   ! get a sort of packet id
  IF pid=0
    @sendACK(pid,adr)     ! Acknowledge reception of packet
  ELSE
    @sendnack(adr)
  ENDIF
  lastadr=adr
  RETURN t$
ENDFUNCTION
