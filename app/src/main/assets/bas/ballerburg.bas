' BALLERBURG "Ubersetzung der original Quelltexte nach X11-Basic V.1.02-1 2009

' Ballerburg is a simple but funny game for two players. Each player manages his
' own castle: one player on the right side, one player on the left side with a
' mountain in between. The two players must shoot their cannons at the other
' player's castle. Ballerburg is a port of the classic Atari ST game.

' This is a port TO X11-Basic, which is very close TO the original version.
' Therefore it comes with the original german manual.
' see ballerburg-anleitung.txt

' Das original Ballerburg wurde 1987 von Eckhard Kruse fuer den Atari ST
' entwickelt.
' Homepage des Orginal-Spiels:
' http://www.eckhardkruse.net/atari_st/baller.html

' Following files are needed:
' baller.dat (castle definition file)
' baller.rsc (original ATARI ST resource file, german)
' baller.tab (will be created)
'
' The soundfiles should be located in sound/
' ballermus.ogg -- The Ballerburg sound (The King was Hit)
'                                       (c) Eckhard Kruse 1987
' die.ogg        explosion.ogg  flail.ogg
' bumm.ogg       einschlag.ogg  flagweg.ogg    horn.ogg
' sounds taken from the wesnoth project.

' http://www.eckhardkruse.net/atari_st/baller.html
' Orginal Ballerburg 29.4.1987  by    Eckhard Kruse
' X11-Basic port by Markus Hoffmann et al. 2009-2013
' https://sourceforge.net/apps/mediawiki/x11-basic/index.php?title=Ballerburg

'
' Notes on this release:
' It is not yet bug free. You are welcomned TO improve the X11-Basic port
' please sent corrections/improvements TO
' Markus Hoffmann <kollo@users.sourceforge.net>
'
' Latest change: 2013-01-22
'
' Select language:
' 0 = german
' 1 = english
' ...
language=0              ! The original was german (0)
dosound=1               ! Use sound

ballerrsc$="baller.rsc" ! The name of the resource file TO use
'
' Nobody has created an english resource file yet...
' IF language=1
'   ballerrsc$="baller-en.rsc"
' ENDIF

IF ANDROID?    ! find out if we are on an ANDROID device.
  basedir$="/mnt/sdcard/bas/"
  rsc$=basedir$+ballerrsc$
  dat$=basedir$+"baller.dat"
  sounddir$=basedir$+"sound/"
  ' sounddir$="/data/data/net.sourceforge.x11basic/"
ELSE
  basedir$="./"
  rsc$=ballerrsc$
  sounddir$=basedir$+"sound/"
  dat$="baller.dat"
ENDIF

@init   ! initialize Menu and data
MENUDEF menue$(),m

IF NOT EXIST(rsc$)
  IF language=1
    ~FORM_ALERT(1,"[3][BALLERBURG cannot find its |"+rsc$+". | It cannot run without.][What a pity!]")
  ELSE
    ~FORM_ALERT(1,"[3][BALLERBURG findet sein |"+rsc$+" nicht.|Ohne gehts leider nicht.][Schade]")
  ENDIF
  QUIT
ENDIF

anbauen_erlaubt=1
mxin=3
max_rund=32767
kann_kapitulieren=1
spiel_modus=0        ! Default Spielmodus 0 bis 4
@prepare

@t_load

@burgen_laden  ! load castle data
l_nam$=spielername1$
r_nam$=spielername2$

' on linux the PLAYSOUND command does not work, so
' try to locate ogg123, which can do the job
IF ANDROID?=0
  IF GLOB(SYSTEM$("which ogg123"),"*ogg123*")=0 AND dosound=1
    DEFMOUSE 0
    IF language=1
      ~FORM_ALERT(1,"[1][The program ogg123 (vorbis tools) |is not installed on your system.|Therefor no sound!.][Pity]")
    ELSE
      ~FORM_ALERT(1,"[1][Sie haben das Programm ogg123 (vorbis tools) |nicht installiert.|Deshalb kein sound!.][Schade]")
    ENDIF
    dosound=0
    DEFMOUSE 2
  ENDIF
ENDIF

IF bw<320 OR bh<240
  ~FORM_ALERT(1,"[3][Der Bildschirm ist zu klein.|Ballerburg wird nicht|richtig funktionieren.][Schade]")
ELSE if bw<480 OR bh<300
  ~FORM_ALERT(1,"[1][Der Bildschirm ist zu klein.|Ballerburg wird wahrscheinlich|nicht richtig funktionieren.][Oh, egal]")
ENDIF

@neues       ! New Game
DEFMOUSE 0
DO
  EXIT IF @ein_zug=0
LOOP
@t_save
@quit
'------------program ends here---------------
PROCEDURE prepare
  DEFMOUSE 2     ! set busybee
  RSRC_LOAD rsc$ ! load original ATARI ST resources

  ' Disable some menu entries

  MENUSET 3,8    ! Accessory 1
  MENUSET 4,8
  MENUSET 5,8
  MENUSET 6,8
  MENUSET 7,8
  MENUSET 8,8    ! Accessory 6
  MENUSET spiel_modus+18,4
RETURN

'
' ************************* Durchfuehren eines Zuges ***************************/
FUNCTION ein_zug
  LOCAL i,fl,a,kanone_adr%,foerderturm_adr%,burg_adr%
  n=zug AND 1
  f=1-2*n
  kn(n)=kn(n) AND (not 16)
  wind=wind*9/10+RANDOM(12)-6
  @werdran(1)
  burg_adr%=VARPTR(burgen$(bur(n)))
  kanone_adr%=VARPTR(kanone$(n))
  foerderturm_adr%=VARPTR(foerderturm$(n))
  '  print "Spieler ";n;" ist dran."
  DO
    fl=0
    DO
      IF @event
        RETURN 0
      ENDIF
      '   for( a=i=0;i<60; ) a|=*(char *)(scr+80+i++);
      EXIT IF bt=1 OR (spiel_modus AND (2-n))
      '      print bt,spiel_modus
    LOOP

    IF spiel_modus AND (2-n)
      @hide
      i=@comp
      @show
      fl=1+abs(i<0)
    ELSE IF mx>ftx AND mx<ftx+ftw AND my>fty AND my<fty+fth
      fl=2
    ELSE
      FOR i=0 TO 9
        EXIT IF LPEEK(kanone_adr%+i*16+0)<=mx AND \
	        LPEEK(kanone_adr%+i*16+0)+20>=mx and \
		LPEEK(kanone_adr%+i*16+0)<>-1 and \
		LPEEK(kanone_adr%+i*16+4)>=my and \
		LPEEK(kanone_adr%+i*16+4)-14<=my
      NEXT i
      IF i>9
        IF @drin(LPEEK(burg_adr%+4*25),LPEEK(burg_adr%+4*26),LPEEK(burg_adr%+4*31),LPEEK(burg_adr%+4*32),0,mx,my) OR \
           @drin(LPEEK(burg_adr%+4*27),LPEEK(burg_adr%+4*28),LPEEK(burg_adr%+4*33),LPEEK(burg_adr%+4*34),0,mx,my) or \
           @drin(LPEEK(burg_adr%+4*29),LPEEK(burg_adr%+4*30),LPEEK(burg_adr%+4*35),LPEEK(burg_adr%+4*36),0,mx,my) 
          @markt
        ELSE IF @drin(LPEEK(burg_adr%+4*21),LPEEK(burg_adr%+4*22),30,25,0,mx,my)
          @koenig
        ELSE
          ' beep
        ENDIF
      ELSE IF pu(n)<5
        BEEP
        ~form_alert(0,"[0][Dein Pulver reicht nicht!][Abbruch]")
      ELSE IF kugeln(n)=0
        BEEP
        ~FORM_ALERT(0,"[0][Du hast keine Kugeln mehr!][Abbruch]")
      ELSE
        fl=@sch_obj(i)
      ENDIF
    ENDIF
    EXIT IF fl<>0
  LOOP
  @werdran(0)
  IF fl<2
    @schuss(i)
  ENDIF
  IF (not kn(n)) AND 16
    kn(n)=kn(n) and (not 15)
  ENDIF
  @rechnen    ! Neue Marktpreise, Volk etc....

  ' Zaehle kanonen
  FOR i=0 TO 9
    EXIT IF LPEEK(kanone_adr%+16*i+0)>-1
  NEXT i

  ! Naechster Spieler ist an der Reihe
  INC zug
  n=zug and 1
  burg_adr%=VARPTR(burgen$(bur(n)))
  kanone_adr%=VARPTR(kanone$(n))
  foerderturm_adr%=VARPTR(foerderturm$(n))
  ' Brechne Lagebewertung --> Kapitulation
  FOR a=0 TO 9
    EXIT IF LPEEK(kanone_adr%+16*a+0)>-1
  NEXT a
  IF a=10 AND i<10 AND LPEEK(burg_adr%+4*40)>volk(n) AND geld(n)<price(2)/3 AND kann_kapitulieren AND \
    LPEEK(foerderturm_adr%+16*0+0)+LPEEK(foerderturm_adr%+1*16+0)+LPEEK(foerderturm_adr%+16*2+0)+LPEEK(foerderturm_adr%+3*16+0)+LPEEK(foerderturm_adr%+16*4+0)=-5
    end=n+33
  ENDIF
  ' Prüfe ob Rundenlimit erreicht ist
  IF end=0 AND zug/2>=max_rund
    ' Berechne, wer besser dran ist, bewerte Mobilien mit aktuellen Marktpreisen.
    DIM h(2)
    FOR n=0 TO 1
      kanone_adr%=VARPTR(kanone$(n))
      foerderturm_adr%=VARPTR(foerderturm$(n))

      h(n)=geld(n)+pu(n)*price(4)/30+kugeln(n)*price(5)/2+abs(wx(n)>-1)*price(3)+volk(n)*4
      FOR i=0 TO 4
        IF LPEEK(foerderturm_adr%+16*i+0)>-1
          h(n)=h(n)+price(1)
        ENDIF
      NEXT i
      FOR i=0 TO 9
        IF LPEEK(kanone_adr%+16*i+0)>-1
          h(n)=h(n)+price(2)
        ENDIF
      NEXT i
    NEXT n
    end=65+abs(h(1)<h(0))
  ENDIF
  IF end
    @ende
    DO
      IF @event
        RETURN 0
      ENDIF
      EXIT IF bt>0 AND end=0
    LOOP
  ENDIF
  RETURN 1
ENDFUNCTION

' ************* Kanonenobjektbaum, Wahl von Winkel und Pulver *****************/
FUNCTION sch_obj(k)
  LOCAL i,wi,pv,xw,yw,xp,yp,xk,yk,kanone_adr%,schussdialog_adr%
  LOCAL s,c,fl
  DIM px(200),py(200)
  ' char *aw,*ap, fl=1;
  fl=1
  schussdialog_adr%=RSRC_GADDR(0,1)

  aw=LPEEK(schussdialog_adr%+24*6+12)
  ap=LPEEK(schussdialog_adr%+24*12+12)
  POKE ap+2,0

  kanone_adr%=VARPTR(kanone$(n))

  ' Ensure that the dialog fits on screen it should fit on 320 pixls width

  IF DPEEK(schussdialog_adr%+20)>bw-9
    DPOKE schussdialog_adr%+20,bw-9
  ENDIF
  IF DPEEK(schussdialog_adr%+22)>bh-9
    DPOKE schussdialog_adr%+22,bh-9
  ENDIF

  ~form_center(schussdialog_adr%,fx,fy,fw,fh)
  ~objc_offset(schussdialog_adr%,6,xw,yw)
  ~objc_offset(schussdialog_adr%,12,xp,yp)
  ~objc_offset(schussdialog_adr%,3,xk,yk)
  ADD xk,105+f*36
  ADD yk,102
  ~form_dial(0,LPEEK(kanone_adr%+16*k+0)+10*f,LPEEK(kanone_adr%+16*k+4)-10,20,14,fx,fy,fw,fh)
  ~form_dial(1,LPEEK(kanone_adr%+16*k+0)+10*f,LPEEK(kanone_adr%+16*k+4)-10,20,14,fx,fy,fw,fh)
  wi=LPEEK(kanone_adr%+16*k+8)
  pv=LPEEK(kanone_adr%+16*k+12)

  '   vsf_interior( handle,1 );
  '   DEFFILL ,1
  DO
    IF pv>pu(n)
      pv=pu(n)
    ENDIF
    POKE aw,48+wi/100
    POKE aw+1,48+(wi MOD 100)/10
    POKE aw+2,48+(wi MOD 10)
    IF wi<100
      POKE aw,PEEK(aw+1)
      POKE aw+1,PEEK(aw+2)
      POKE aw+2,0
    ENDIF
    POKE ap,48+pv/10
    POKE ap+1,48+(pv MOD 10)
    IF pv<10
      POKE ap,PEEK(ap+1)
      POKE ap+1,0
    ENDIF

    IF fl OR 1
      ~objc_draw(schussdialog_adr%,0,-1,0,0,0,0)
      @hide
      COLOR schwarz
      PCIRCLE xk-88*f,yk-60,15
      i=0
      WHILE LPEEK(VARPTR(fig$)+i*4)<>-1
        px(INT(i/2))=xk-88*f+LPEEK(VARPTR(fig$)+i*4)
        INC i
        py(INT(i/2))=yk-5+LPEEK(VARPTR(fig$)+i*4)
        INC i
      WEND
      px(INT(i/2))=px(0)
      py(INT(i/2))=py(0)
      INC i
      POLYFILL int(i/2),px(),py()
      @show()
    ENDIF
    '      ~objc_draw(schussdialog_adr%,6,6,0,0,0,0)
    '      ~objc_draw(schussdialog_adr%,12,12,0,0,0,0)
    '     ~objc_draw(schussdialog_adr%,0,-1,0,0,0,0)

    IF i<>15 AND i<>16 AND i<>13 AND i<>13 OR fl<>0
      @hide
      @clr(xk-55,yk-76,110,90)
      COLOR schwarz
      PCIRCLE xk,yk,15

      s=sin(wi/57.296)
      c=cos(wi/57.296)
      fl=-f
      IF wi>90
        fl=-fl
        c=-c
      ENDIF
      px(0)=xk+fl*(c*14+s*14)
      py(0)=yk+s*14-c*14
      px(1)=xk+fl*(c*14+s*40)
      py(1)=yk+s*14-c*40
      px(2)=xk-fl*(c*55-s*40)
      py(2)=yk-s*55-c*40
      px(3)=xk-fl*(c*55-s*14)
      py(3)=yk-s*55-c*14
      px(4)=px(0)
      py(4)=py(0)
      POLYFILL 4,px(),py()
      @show
      fl=0
    ENDIF
    i=FORM_DO(schussdialog_adr%)
    IF i>=0
      DPOKE schussdialog_adr%+24*i+10,0
    ENDIF
    SUB wi,10*abs(i=9)-10*abs(i=10)+abs(i=8)-abs(i=7)
    wi=MAX(MIN(wi,180),0)
    SUB pv,3*abs(i=15)-3*abs(i=16)+abs(i=13)-abs(i=14)
    pv=MAX(MIN(pv,20),5)
    EXIT IF i=1 OR i=2
  LOOP
  ~form_dial(2,LPEEK(kanone_adr%+16*k+0)+10*f,LPEEK(kanone_adr%+16*k+4)-10,20,14,fx,fy,fw,fh)
  ~form_dial(3,LPEEK(kanone_adr%+16*k+0)+10*f,LPEEK(kanone_adr%+16*k+4)-10,20,14,fx,fy,fw,fh)
  LPOKE kanone_adr%+16*k+8,wi
  LPOKE kanone_adr%+16*k+12,pv
  RETURN abs(i=1)
ENDFUNCTION

PROCEDURE pokestring(adr%,t$,n)
  t$=t$+CHR$(0)
  IF len(t$)>n
    POKE varptr(t$)+n,0
  ENDIF
  BMOVE varptr(t$),adr%,MIN(n,LEN(t$))
RETURN
FUNCTION peekstring$(adr%,n)
  LOCAL t$,a$
  t$=SPACE$(n)
  BMOVE adr%,VARPTR(t$),n
  SPLIT t$,CHR$(0),0,t$,a$
  RETURN t$
ENDFUNCTION

' /** Anzeige des Spielers, der am Zug ist, sowie Darstellung der Windfahnen ***/
PROCEDURE werdran(c)
  LOCAL a,ad,winddialog_adr%
  LOCAL i,x,y,w,h,c1,s1,c2,s2
  LOCAL wk,wl,t$
  DIM px(6),py(6)

  txt$=@z_txt$(zug/2+1)
  ' COLOR weiss
  ' pbox bw/2+10,by+bh-5,bw/2+60,by+bh-20
  COLOR schwarz,weiss
  TEXT bw/2+12,by+bh-5,txt$
  winddialog_adr%=RSRC_GADDR(0,4)
  a=winddialog_adr%+16

  IF c
    IF werdranflag
      @werdran(0)
    ENDIF
    ' Position des Fensters
    DPOKE a,5+(bw-11-dpeek(a+4))*n
    DPOKE a+2,by+2
    IF n=0
      @pokestring(LPEEK(winddialog_adr%+2*24+12),l_nam$,20)
    ELSE
      @pokestring(LPEEK(winddialog_adr%+2*24+12),r_nam$,20)
    ENDIF
    ad=LPEEK(winddialog_adr%+4*24+12)
    IF wx(n)<0            ! keine Windfahne vorhanden
      POKE ad,ASC(" ")
      POKE ad+5,ASC(" ")
      POKE ad+2,ASC("?")
      POKE ad+3,ASC("?")
    ELSE
      POKE ad,ASC(">")*abs(wind>0)+ASC("<")*abs(wind<0)+32*abs(wind=0)
      POKE ad+5,ASC(">")*abs(wind>0)+ASC("<")*abs(wind<0)+32*abs(wind=0)
      POKE ad+2,48+(abs(wind) DIV 10)
      POKE ad+3,48+(abs(wind) MOD 10)
    ENDIF
    ~objc_offset(winddialog_adr%,7,ftx,fty)
    ftw=dpeek(winddialog_adr%+7*24+20)
    fth=dpeek(winddialog_adr%+7*24+22)
    ~objc_offset(winddialog_adr%,6,x,y)
    w=dpeek(winddialog_adr%+6*24+20)
    h=dpeek(winddialog_adr%+6*24+22)
    ~FORM_DIAL(0,0,0,0,0,dpeek(a),dpeek(a+2),dpeek(a+4),dpeek(a+6))
    ~FORM_DIAL(1,0,0,0,0,dpeek(a),dpeek(a+2),dpeek(a+4),dpeek(a+6))

    ~objc_draw(winddialog_adr%,0,4,0,0,0,0)
    c=sgn(wind)
    wk=abs(wind)/15.0
    wl=wk*0.82
    wk=MIN(wk,pi/2)
    wl=MIN(wl,pi/2)
    s1=c*20*sin(wk)
    c1=20*cos(wk)
    s2=c*20*sin(wl)
    c2=20*cos(wl)
    ws=s1/2.0
    '  add ws,abs(-(ws+1))
    wc=c1/2.0
    @hide
    IF wx(n)>-1
      COLOR schwarz
      ADD x,w/2
      LINE x,y+h,x,y+5
      LINE x+1,y+h,x+1,y+5
      px(0)=x+1
      px(1)=x+1
      py(0)=y+5
      py(1)=y+11
      IF wk<0.2
        px(0)=x-1
        py(1)=y+5
        px(1)=x+2
      ENDIF
      px(2)=px(1)+s1
      py(2)=py(1)+c1
      px(4)=px(0)+s1
      py(4)=py(0)+c1
      px(5)=px(0)
      py(5)=py(0)
      px(3)=(px(2)+px(4))/2+s2
      py(3)=(py(2)+py(4))/2+c2
      POLYLINE 6,px(),py()
    ENDIF
    @fahne
    @show
    werdranflag=true
  ELSE
    @hide
    ~FORM_DIAL(2,0,0,0,0,dpeek(a),dpeek(a+2),dpeek(a+4),dpeek(a+6))
    ~FORM_DIAL(3,0,0,0,0,dpeek(a),dpeek(a+2),dpeek(a+4),dpeek(a+6))
    ' @clr(DPEEK(a),DPEEK(a+2),DPEEK(a+4)+1,DPEEK(a+6)+1)
    @show
    werdranflag=false
  ENDIF
RETURN
' Clear a region of the screen
PROCEDURE clr(x,y,w,h)
  COLOR weiss
  PBOX x,y,x+w-1,y+h-1
RETURN
' /******************* Darstellung der beiden Windfahnen ***********************/
' wx(),wy() Data of Fahne
' ws,wc -- coordinates of the big fahne
' n     -- 0 or 1 depending on who is dran
PROCEDURE fahne
  LOCAL m
  FOR m=0 TO 1
    IF wx(m)>-1
      @clr(wx(m)-10,wy(m)-15,20,15)
      COLOR schwarz
      LINE wx(m),wy(m),wx(m),wy(m)-15
      IF m=n
        LINE wx(m),wy(m)-15,wx(m)+ws,wy(m)-13+wc
        LINE wx(m),wy(m)-11,wx(m)+ws,wy(m)-13+wc
      ENDIF
    ENDIF
  NEXT m
RETURN

' /************************* BALLER.DAT laden **********************************/
' fills the array burgend$(b_anz) with drawing data of the castles
'
PROCEDURE burgen_laden
  LOCAL j
  IF NOT EXIST(dat$)
    IF language=1
      ~FORM_ALERT(1,"[1][Cannot find  BALLER.DAT !][ABORT]")
    ELSE
      ~FORM_ALERT(1,"[1][Kann BALLER.DAT nicht finden.][Abbruch]")
    ENDIF
    QUIT
  ENDIF
  OPEN "I",#1,dat$
  b_anz=0
  j=@rdzahl
  WHILE j<>-999
    b$=mkl$(j)
    FOR j=0 TO 39
      b$=b$+MKL$(@rdzahl)
    NEXT j
    j=@rdzahl
    WHILE j<>-1
      b$=b$+MKL$(j)
      j=@rdzahl
    WEND
    burgen$(b_anz)=b$+mkl$(-1)
    INC b_anz
    j=@rdzahl
  WEND
  ' print b_anz;" Burgen geladen."
  CLOSE #1
RETURN

' liest eine Dezimalzahl von der Datei, Remarks werden ueberlesen */
FUNCTION rdzahl
  LOCAL val,a,sign,rem
  sign=1
  rem=0    ! wird durch * getoggled, und zeigt damit an,  ob man sich in einer Bemerkung befindet */
  val=0
  IF EOF(#1)
    RETURN -1
  ENDIF
  DO
    a=INP(#1)
    IF a=ASC("*")
      rem=NOT rem
    ENDIF
    EXIT IF rem=0 AND (a=ASC("-") OR (a>=ASC("0") AND a<=ASC("9")))
  LOOP
  IF a=ASC("-")
    sign=-1
    a=INP(#1)
  ENDIF
  WHILE a>=ASC("0") AND a<=ASC("9")
    val=val*10
    ADD val,a-ASC("0")
    a=INP(#1)
  WEND
  RETURN sign*val
ENDFUNCTION

' ********************** BALLER.TAB laden/speichern ***************************/
PROCEDURE t_load
  LOCAL t$,adr,i
  IF EXIST("baller.tab")
    ' print "<- baller.tab"
    OPEN "I",#1,"baller.tab"
    anbauen_erlaubt=INP(#1)
    kann_kapitulieren=INP(#1)
    mxin=INP(#1)
    t$=INPUT$(#1,3)
    adr=RSRC_GADDR(0,13)
    BMOVE VARPTR(t$),LPEEK(LPEEK(adr+6*24+12)),3
    ' print "t-load: ";LEFT$(t$,3)
    max_rund=((INP(#1)*256) AND -256)+(INP(#1) AND 255)
    FOR i=0 TO 5
      tabellen_name$(i)=INPUT$(#1,8)
      ' print "name:";tabellen_name$(i)
    NEXT i
    t_gew$=INPUT$(#1,120)
    CLOSE #1
    ' MEMDUMP varptr(t_gew$),120
    ' print "t-load: max-rund ";max_rund
  ENDIF
RETURN
PROCEDURE t_save
  LOCAL t$,i
  ' print "--> baller.tab"
  OPEN "O",#1,"baller.tab"
  t$=SPACE$(3)
  BMOVE LPEEK(LPEEK(RSRC_GADDR(0,13)+6*24+12)),VARPTR(t$),3
  PRINT #1,CHR$(anbauen_erlaubt)+CHR$(kann_kapitulieren)+CHR$(mxin)+t$+REVERSE$(MKI$(max_rund));
  FOR i=0 TO 5
    PRINT #1,LEFT$(tabellen_name$(i)+STRING$(8,CHR$(0)),8);
  NEXT i
  PRINT #1,LEFT$(t_gew$+STRING$(120,CHR$(0)),120);
  CLOSE #1
RETURN

FUNCTION event
  MENU
  PAUSE 0.05
  MOUSE mx,my,bt
  RETURN 0
ENDFUNCTION

PROCEDURE init
  LOCAL i
  DIM menue$(80)
  DIM burg_x(2),burg_y(2),steuersatz(2),kn(2),wx(2),wy(2),bur(2),geld(2),pu(2),kugeln(2),volk(2)
  DIM cw(2),cx(2)
  DIM price(6)
  DIM tabellen_name$(6),computer_name$(7)
  DIM kanone$(2),foerderturm$(2)
  ARRAYFILL cw(),2
  ARRAYFILL cx(),1

  DIM burgen$(100)

  kanone$(0)=STRING$(4*10,mkl$(-1))
  foerderturm$(0)=STRING$(4*10,mkl$(-1))
  kanone$(1)=STRING$(4*10,mkl$(-1))
  foerderturm$(1)=STRING$(4*10,mkl$(-1))

  tabellen_name$()=["Tölpel","Dummel","Brubbel","Wusel","Brösel","Töffel"]
  computer_name$()=["Tölpel","Dummel","Brubbel","Wusel","Brösel","Töffel","Rüpel"]
  spielername1$="Hugo"
  spielername2$="Emil"
  DIM ltz$(2)
  '
  rot=GET_COLOR(65530,0,0)
  gelb=GET_COLOR(65530,40000,0)
  grau=GET_COLOR(65530/2,65530/2,65530/2)
  weiss=GET_COLOR(65530,65530,65530)
  schwarz=GET_COLOR(0,0,0)
  lila=GET_COLOR(65530,0,65530)
  blau=GET_COLOR(10000,10000,65530)
  gruen=GET_COLOR(0,30000,0)
  bx=0
  SHOWPAGE
  bw=320*2
  bh=400
  GET_GEOMETRY 1,bx,by,bw,bh
  IF ANDROID?
    ' find out which text hight we have
    by=bh/rows
    PRINT "by=";by
  ELSE
    by=16
  ENDIF
  bh=bh-by
  COLOR weiss
  PBOX bx,by+1,bx+bw,by+bh
  COLOR schwarz
  DEFFILL 1,2,0

  ' Load the menu definitions
  IF language=1
    RESTORE menudata_en
  ELSE
    RESTORE menudata
  ENDIF
  FOR i=0 TO 80
    READ t$
    menue$(i)=t$
    EXIT IF menue$(i)="***"
  NEXT i
  menue$(i)=""
  menue$(i+1)=""
  SHOWPAGE

  RESTORE trohn
  READ n
  trohn$=""
  WHILE n<>-1
    trohn$=trohn$+mkl$(n)
    READ n
  WEND
  trohn$=trohn$+mkl$(-1)
  RESTORE kanon
  READ n
  kanon$=""
  WHILE n<>-1
    kanon$=kanon$+mkl$(n)
    READ n
  WEND
  kanon$=kanon$+mkl$(-1)
  RESTORE sack
  sack$=@rd$()
  RESTORE fass
  fass$=@rd$()
  RESTORE kuge
  kuge$=@rd$()
  RESTORE turm
  turm$=@rd$()
  RESTORE fig
  fig$=@rd$()+mkl$(-1)
  RESTORE sprueche
  READ kna$,kne$
  READ kn0$,kn1$,kn2$,kn3$,kn4$,kn5$,kn6$,kn7$,kn8$,kn9$,kn10$
  READ kn11$,kn12$,kn13$,kn14$
RETURN
FUNCTION rd$()
  LOCAL t$,n
  t$=""
  READ n
  WHILE n<>-1
    t$=t$+MKL$(n)
    READ n
  WEND
  t$=t$+MKL$(-1)
  RETURN t$
ENDFUNCTION

' cleanup and quit program
PROCEDURE quit
  RSRC_FREE
  QUIT
RETURN

' Initialize a new game

PROCEDURE neues
  standard_prices()=[200,500,400,150,50,50]   ! Preise zu Beginn
  wind=RANDOM(60)-30
  steuersatz(0)=20
  steuersatz(1)=20
  kn(0)=0
  kn(1)=0
  FOR j=0 TO 5
    price(j)=standard_prices(j)*(95+RANDOM(11))/100
  NEXT j
  @bild
  FOR n=0 TO 1
    burg_adr%=VARPTR(burgen$(bur(n)))
    IF n
      wx(n)=bw-1-LPEEK(burg_adr%+4*23)
    ELSE
      wx(n)=LPEEK(burg_adr%+4*23)
    ENDIF
    wy(n)=burg_y(n)-LPEEK(burg_adr%+4*24)
    foerderturm$(n)=STRING$(5*4,MKL$(-1))
  NEXT n
  CLR zug,n,end
  f=1
  @dosound("horn.ogg")
RETURN
' **************************** Neues Bild zeichnen ****************************/
PROCEDURE bild
  LOCAL y,x1,x2,v1,v2
  @hide
  @cls
  burg_y(0)=Random(84)+((by+bh-100) AND (not 3))
  burg_y(1)=Random(84)+((by+bh-100) AND (not 3))

  y=by+bh
  x1=0
  x2=(bw-1)*4
  COLOR rot
  v1=2
  v2=2
  ' vsl_type( handle,7 );
  DEFFILL ,2,7
  WHILE x1<x2 AND y>20
    '  vsl_udsty( handle,~((257<<(Random()&7))*(y and 1)) )
    DEFFILL ,2,RANDOM(4)+7
    LINE x1/4,y,x2/4,y
    IF y=burg_y(0)
      x1=LPEEK(VARPTR(burgen$(bur(0))))*4
    ENDIF
    IF y=burg_y(1)
      x2=(bw-1-LPEEK(VARPTR(burgen$(bur(1)))))*4
    ENDIF
    IF x1
      v1=v1+Random(5)-2
      v1=Max(v1,0)
      v1=Min(v1,7)
      ADD x1,v1
    ENDIF
    IF x2<(bw-1)*4
      v2=v2+Random(5)-2
      v2=Max(v2,0)
      v2=Min(v2,7)
      SUB x2,v2
    ENDIF
    DEC y
  WEND
  SHOWPAGE
  '  vsl_type( handle,1 );
  kanone$(0)=STRING$(40,mkl$(-1))
  kanone$(1)=STRING$(40,mkl$(-1))
  DEFFILL ,2,0
  @burg(0)
  @burg(1)

  COLOR weiss
  PBOX bw/2-44,by+bh-5,bw/2+44,by+bh-5-16
  COLOR schwarz,weiss
  TEXT bw/2-44,by+bh-5," Runde     "
  @show
RETURN

PROCEDURE n
  ~form_alert(1,"[3][Funktion nicht implementiert !][ OH ]")
RETURN
PROCEDURE m(k)
  ' print k
  ON k gosub info
  ON k-10 gosub neues_spiel,name_input,optionen,n,quit
  ON k-22 gosub computerwahl,n,n,spielregeln,n,n,tname_input,t_load,t_save
  ON k-32 gosub show_tabelle
  IF k>=18 AND k<=21
    spiel_modus=k-18
    MENUSET 18,0
    MENUSET 19,0
    MENUSET 20,0
    MENUSET 21,0
    MENUSET k,4
  ENDIF
RETURN
PROCEDURE neues_spiel
  IF @bur_obj            ! Auswahl der Burgen
    @neues
    @werdran(1)
  ENDIF
  GRAPHMODE 1
  COLOR schwarz,weiss
  DEFFILL 1,2,0
RETURN
PROCEDURE name_input
  LOCAL adr%,adr3,adr4,x,y,w,h,ret%
  adr%=RSRC_GADDR(0,5)
  adr3=LPEEK(LPEEK(adr%+2*24+12))
  @pokestring(adr3,spielername1$,20)
  adr4=LPEEK(LPEEK(adr%+3*24+12))
  @pokestring(adr4,spielername2$,20)
  ~FORM_CENTER(adr%,x,y,w,h)
  ~FORM_DIAL(0,x,y,w,h,x,y,w,h)
  ~FORM_DIAL(1,x,y,w,h,x,y,w,h)
  ~OBJC_DRAW(adr%,0,-1,0,0,0,0)
  ret%=FORM_DO(adr%)
  ~FORM_DIAL(2,x,y,w,h,x,y,w,h)
  ~FORM_DIAL(3,x,y,w,h,x,y,w,h)
  IF ret%>3 AND ret%<6
    DPOKE adr%+ret%*24+10,0
  ENDIF
  IF ret%=4
    spielername1$=@peekstring$(adr3,20)
    spielername2$=@peekstring$(adr4,20)
    l_nam$=spielername1$
    r_nam$=spielername2$
    IF end=0
      @werdran(1)
    ENDIF
  ENDIF
RETURN
PROCEDURE tname_input
  LOCAL adr%,ret%,x,y,w,h
  adr%=RSRC_GADDR(0,12)

  DPOKE adr%+24*13+10,DPEEK(adr%+24*12+10) XOR 1

  LPOKE LPEEK(adr%+24*(5+0)+12),VARPTR(tabellen_name$(0))
  LPOKE LPEEK(adr%+24*(5+1)+12),VARPTR(tabellen_name$(1))
  LPOKE LPEEK(adr%+24*(5+2)+12),VARPTR(tabellen_name$(2))
  LPOKE LPEEK(adr%+24*(5+3)+12),VARPTR(tabellen_name$(3))
  LPOKE LPEEK(adr%+24*(5+4)+12),VARPTR(tabellen_name$(4))
  LPOKE LPEEK(adr%+24*(5+5)+12),VARPTR(tabellen_name$(5))

  ~FORM_CENTER(adr%,x,y,w,h)
  ~FORM_DIAL(0,x,y,w,h,x,y,w,h)
  ~FORM_DIAL(1,x,y,w,h,x,y,w,h)
  ~OBJC_DRAW(adr%,0,-1,0,0,0,0)
  ret%=FORM_DO(adr%)
  ~FORM_DIAL(2,x,y,w,h,x,y,w,h)
  ~FORM_DIAL(3,x,y,w,h,x,y,w,h)
  IF ret%>0 AND ret%<20
    DPOKE adr%+ret%*24+10,0
  ENDIF

  IF ret%=1
    doloesch=DPEEK(adr%+24*12+10) AND 1
    IF doloesch=1
      t_gew$=STRING$(120,CHR$(0))
    ENDIF
  ENDIF
RETURN
PROCEDURE optionen
  LOCAL adr%,ret%,x,y,w,h,hh
  adr%=RSRC_GADDR(0,13)
  ~FORM_CENTER(adr%,x,y,w,h)
  ~FORM_DIAL(0,x,y,w,h,x,y,w,h)
  ~FORM_DIAL(1,x,y,w,h,x,y,w,h)
  DPOKE adr%+9*24+10,ABS(anbauen_erlaubt>0)
  DPOKE adr%+10*24+10,ABS(anbauen_erlaubt=0)
  DPOKE adr%+3*24+10,ABS(mxin=0)
  DPOKE adr%+4*24+10,ABS(mxin=1)
  DPOKE adr%+6*24+10,ABS(mxin=2)
  DPOKE adr%+5*24+10,ABS(mxin=3)
  DPOKE adr%+15*24+10,kann_kapitulieren
  ~OBJC_DRAW(adr%,0,-1,0,0,0,0)
  ret%=FORM_DO(adr%)
  IF ret%=11
    DPOKE adr%+ret%*24+10,0
    kann_kapitulieren=DPEEK(adr%+15*24+10)
    anbauen_erlaubt=DPEEK(adr%+9*24+10)
    mxin=DPEEK(adr%+4*24+10)+2*DPEEK(adr%+6*24+10)+3*DPEEK(adr%+5*24+10)
    hh=LPEEK(adr%+6*24+12)
    max_rund=PEEK(hh)-48
    IF PEEK(hh+1)
      max_rund=max_rund*10+PEEK(hh+1)-48
      IF PEEK(hh+2)
        max_rund=max_rund*10+PEEK(hh+2)-48
      ENDIF
    ENDIF
    IF mxin=0
      max_rund=20
    ENDIF
    IF mxin=1
      max_rund=50
    ENDIF
    IF mxin=3
      max_rund=32767
    ENDIF
  ENDIF
  ~FORM_DIAL(2,x,y,w,h,x,y,w,h)
  ~FORM_DIAL(3,x,y,w,h,x,y,w,h)
RETURN
PROCEDURE show_tabelle
  LOCAL ox,oy
  ox=(bx+bw-532)/2
  oy=(by+bh-242)/2
  ~FORM_DIAL(0,260,20,30,20,ox,oy,532,242)
  ~FORM_DIAL(1,260,20,30,20,ox,oy,532,242)
  @tabelle(ox,oy,532,242)
  MOUSEEVENT
  ~FORM_DIAL(2,260,20,30,20,ox,oy,532,242)
  ~FORM_DIAL(3,260,20,30,20,ox,oy,532,242)
RETURN
PROCEDURE info
  LOCAL adr%,x,y,w,h,ret%
  adr%=RSRC_GADDR(0,2)
  ~FORM_CENTER(adr%,x,y,w,h)
  ~FORM_DIAL(0,x,y,w,h,x,y,w,h)
  ~FORM_DIAL(1,x,y,w,h,x,y,w,h)
  ~OBJC_DRAW(adr%,0,-1,0,0,0,0)
  ret%=FORM_DO(adr%)
  ~FORM_DIAL(2,x,y,w,h,x,y,w,h)
  ~FORM_DIAL(3,x,y,w,h,x,y,w,h)
  IF ret%=35
    DPOKE adr%+ret%*24+10,0
  ENDIF
RETURN

PROCEDURE hide
  ' hidem
RETURN
PROCEDURE show
  ' showm
  SHOWPAGE
RETURN
PROCEDURE cls
  GRAPHMODE 1
  DEFFILL 1,2,0
  COLOR weiss,weiss
  PBOX bx,by,bx+bw,by+bh
RETURN

PROCEDURE spielregeln
  LOCAL a
  a=@spielregeln1
  WHILE a>0 AND a<4
    IF a=1
      a=@spielregeln1
    ELSE IF a=2
      a=@spielregeln2
    ELSE IF a=3
      a=@spielregeln3
    ENDIF
  WEND
RETURN
FUNCTION spielregeln1
  LOCAL a%,adr%,x,y,w,h
  adr%=RSRC_GADDR(0,9)
  ~form_center(adr%,x,y,w,h)
  ~form_dial(0,x,y,w,h,x,y,w,h)
  ~form_dial(1,x,y,w,h,x,y,w,h)
  ~objc_draw(adr%,0,-1,0,0,0,0)
  a%=FORM_DO(adr%)
  ~form_dial(2,x,y,w,h,x,y,w,h)
  ~form_dial(3,x,y,w,h,x,y,w,h)
  IF a%>29 AND a%<33
    DPOKE adr%+a%*24+10,0
  ENDIF
  IF a%=32
    RETURN 2
  ELSE IF a%=31
    RETURN 3
  ELSE IF a%=30
    RETURN 0
  ELSE
    RETURN a%
  ENDIF
ENDFUNCTION
FUNCTION spielregeln2
  LOCAL a%,adr%,x,y,w,h
  adr%=RSRC_GADDR(0,10)
  ~form_center(adr%,x,y,w,h)
  ~form_dial(0,x,y,w,h,x,y,w,h)
  ~form_dial(1,x,y,w,h,x,y,w,h)
  ~objc_draw(adr%,0,-1,0,0,0,0)
  a=FORM_DO(adr%)
  ~form_dial(2,x,y,w,h,x,y,w,h)
  ~form_dial(3,x,y,w,h,x,y,w,h)
  IF a%>27 AND a%<33
    DPOKE adr%+a%*24+10,0
  ENDIF
  IF a%=30
    RETURN 1
  ELSE IF a%=29
    RETURN 3
  ELSE IF a%=28
    RETURN 0
  ELSE
    RETURN a%
  ENDIF
ENDFUNCTION
FUNCTION spielregeln3
  LOCAL a%,adr%,x,y,w,h
  adr%=RSRC_GADDR(0,11)
  ~form_center(adr%,x,y,w,h)
  ~form_dial(0,x,y,w,h,x,y,w,h)
  ~form_dial(1,x,y,w,h,x,y,w,h)
  ~objc_draw(adr%,0,-1,0,0,0,0)
  a=FORM_DO(adr%)
  ~form_dial(2,x,y,w,h,x,y,w,h)
  ~form_dial(3,x,y,w,h,x,y,w,h)
  IF a%>27 AND a%<33
    DPOKE adr%+a%*24+10,0
  ENDIF
  IF a%=30
    RETURN 1
  ELSE IF a%=29
    RETURN 2
  ELSE IF a%=28
    RETURN 0
  ELSE
    RETURN a%
  ENDIF
ENDFUNCTION

FUNCTION z_txt$(a)
  LOCAL t$
  t$=CHR$(INT(a/100)+48)+CHR$(INT((a MOD 100)/10)+48)+CHR$(INT(a MOD 10)+48)
  IF a<100
    POKE varptr(t$),32
    IF a<10
      POKE varptr(t$)+1,32
    ENDIF
  ENDIF
  ' for(a=0;a<3;a++) txt[a]=txt[a]==48? 79: txt[a]==49? 108: txt[a];
  RETURN t$
ENDFUNCTION

' /**************************** Tabelle ****************************************/
PROCEDURE tabelle(x,y,w,h)
  LOCAL i,j,txt$
  @dosound("horn.ogg")
  @hide
  ' vsf_interior(handle,0);
  COLOR weiss,weiss
  PBOX x,y,x+w,y+h
  COLOR schwarz,weiss
  BOX x,y,x+w,y+h
  BOX x+2,y+2,x+w-2,y+h-2
  BOX x+3,y+3,x+w-3,y+h-3

  LINE x+88,y+4,x+88,y+h-4
  LINE x+88+8,y+4,x+88+8,y+h-4
  FOR i=x+96 TO x+w-1 STEP 72
    LINE i,y+4,i,y+h-4
  NEXT i
  LINE x+4,y+24,x+w-4,y+24
  LINE x+4,y+24+8,x+w-4,y+24+8
  LINE x+4,y+24+6*20+8,x+w-4,y+24+6*20+8
  FOR i=0 TO 10
    LINE x+4,y+32+i*20+8*abs(i>5),x+w-4,y+32+i*20+8*abs(i>5)
  NEXT i
  FOR i=0 TO 5
    TEXT x+96+8+i*72,y+20,tabellen_name$(i)
  NEXT i
  FOR i=0 TO 5
    TEXT x+8,y+48+i*20,tabellen_name$(i)
  NEXT i

  FOR i=0 TO 5
    FOR j=0 TO 9
      txt$=@z_txt$(LPEEK(VARPTR(t_gew$)+4*(i*10+j)))
      IF j=9 AND (not LPEEK(VARPTR(t_gew$)+4*(i*10+6)))
        txt$=" -"
      ENDIF
      TEXT x+96+24+i*72,y+48+j*20+8*abs(j>5),txt$
      IF i=j
        DEFFILL ,2,2
        PBOX x+96+i*72,y+32+j*20,x+96+72+i*72,y+52+j*20
        DEFFILL ,2,0
        BOX x+96+i*72,y+32+j*20,x+96+72+i*72,y+52+j*20
      ENDIF
    NEXT j
  NEXT i
  TEXT x+8,y+32+6*20+8+16,"#  Spiele"
  TEXT x+8,y+32+6*20+8+36,"# gewonnen"
  TEXT x+8,y+32+6*20+8+56,"# verloren"
  TEXT x+8,y+32+6*20+8+76,"Siege in %"
  LINE x+4,y+4,x+88,y+24
  ' setfont "*lucida-bold-r-*-*-8-*"
  SETFONT "5x7"
  TEXT x+8,y+22,"VERLOREN"
  TEXT x+46,y+13,"GEWONNEN"
  ' setfont "*lucida-bold-r-*-*-14-*"
  SETFONT "8x16"
  @show
RETURN

' /******************************* Spielende ***********************************/
PROCEDURE ende
  ' char s1[80],s2[80],s3[80]
  LOCAL a,b,c,s1$,s2$,s3$,siegdialog_adr%
  ' int sav_ssp;

  siegdialog_adr%=RSRC_GADDR(0,7)
  ~FORM_CENTER(siegdialog_adr%,fx,fy,fw,fh)
  DPOKE siegdialog_adr%+18,30

  s1$="!! "
  IF end AND 2
    s1$=s1$+l_nam$
  ELSE
    s1$=s1$+r_nam$
  ENDIF
  s1$=s1$+" hat gewonnen !!"
  s2$=""

  IF (NOT end) AND 64
    s2$=s2$+"( "
  ENDIF
  IF end AND 2
    s2$=s2$+l_nam$
  ELSE
    s2$=s2$+r_nam$
  ENDIF

  IF (end AND 240)<48
    IF RIGHT$(s2$)="s"
      s2$=s2$+"' "
    ELSE
      s2$=s2$+"s "
    ENDIF
  ENDIF
  IF (end AND 240)=16
    s2$=s2$+"Koenig wurde getroffen,"
    s3$="  daraufhin ergab sich dessen Volk. )"
  ELSE IF (end AND 240)=32
    s2$=s2$+"Koenig hat aufgrund der"
    s3$="  aussichtslosen Lage kapituliert. )"
  ELSE IF (end AND 240)=48
    s2$=s2$+" hat kein Volk mehr. )"
    s3$=""
  ELSE IF (end AND 240)=64
    s3$=s2$
    s2$="( Die maximale Rundenzahl ist erreicht."
    s3$=s3$+" befindet sich in der schlechteren Lage. )"
  ENDIF
  adr3=LPEEK(siegdialog_adr%+24*6+12)
  ' MEMDUMP adr3,64

  s1$=s1$+CHR$(0)
  s2$=s2$+CHR$(0)
  s3$=s3$+CHR$(0)

  LPOKE siegdialog_adr%+24*6+12,VARPTR(s1$)
  LPOKE siegdialog_adr%+24*8+12,VARPTR(s2$)
  LPOKE siegdialog_adr%+24*9+12,VARPTR(s3$)

  @dosound("ballermus.ogg")
  PAUSE 1
  ~objc_draw(siegdialog_adr%,0,-1,0,0,0,0)
  SHOWPAGE
  FOR a=0 TO 5
    EXIT IF LEFT$(tabellen_name$(a),7)=LEFT$(l_nam$,7)
  NEXT a
  FOR b=0 TO 5
    EXIT IF LEFT$(tabellen_name$(b),7)=LEFT$(r_nam$,7)
  NEXT b
  IF a<6 AND b<6 AND a<>b
    IF ((not end) AND 2)
      c=a
      a=b
      b=c
    ENDIF
    LPOKE varptr(t_gew$)+4*(a*10+b),LPEEK(VARPTR(t_gew$)+4*(a*10+b))+1
    LPOKE varptr(t_gew$)+4*(b*10+8),LPEEK(VARPTR(t_gew$)+4*(b*10+8))+1
    LPOKE varptr(t_gew$)+4*(a*10+7),LPEEK(VARPTR(t_gew$)+4*(a*10+7))+1
    LPOKE varptr(t_gew$)+4*(a*10+6),LPEEK(VARPTR(t_gew$)+4*(a*10+6))+1
    LPOKE varptr(t_gew$)+4*(b*10+6),LPEEK(VARPTR(t_gew$)+4*(b*10+6))+1

    LPOKE varptr(t_gew$)+4*(a*10+9),100*LPEEK(VARPTR(t_gew$)+4*(a*10+7))/LPEEK(VARPTR(t_gew$)+4*(a*10+6))
    LPOKE varptr(t_gew$)+4*(b*10+9),100*LPEEK(VARPTR(t_gew$)+4*(b*10+7))/LPEEK(VARPTR(t_gew$)+4*(b*10+6))
  ENDIF
  @hide
  ' sav_ssp=Super(0); m_musik(); Super(sav_ssp); show();  /* Musik... */
  ' Giaccess( 0,138 ); Giaccess( 0,139 ); Giaccess( 0,140 );
RETURN

' *************** Burg zeichnen ******************
PROCEDURE burg(nn)
  LOCAL i,xr
  ' print "Zeichne Burg #";nn
  oldn=n
  n=nn
  f=1-2*(nn AND 1)
  IF n AND 2
    n=n and 1
    xr=burg_x(n)
  ELSE
    xr=(bw-1)*n
  ENDIF
  burg_adr%=VARPTR(burgen$(bur(n)))

  geld(n)=LPEEK(burg_adr%+4*37)
  pu(n)=LPEEK(burg_adr%+4*38)
  kugeln(n)=LPEEK(burg_adr%+4*39)
  volk(n)=LPEEK(burg_adr%+4*40)

  COLOR schwarz

  GRAPHMODE 1
  @draw(xr,burg_y(n),burg_adr%+4*45)
  GRAPHMODE 1

  @clr(xr+f*LPEEK(burg_adr%+4*21)-n*30,burg_y(n)-LPEEK(burg_adr%+4*22)-25,30,25)
  COLOR schwarz

  @draw(xr+f*LPEEK(burg_adr%+4*21),burg_y(n)-LPEEK(burg_adr%+4*22),varptr(trohn$))
  FOR i=0 TO 9
    EXIT IF LPEEK(burg_adr%+4*(i*2+1))<=-1
    @init_ka(n,i,xr)
  NEXT i
  n=nn
  @drw_all
  n=oldn
  IF 0
    PRINT "Kanonen für Spieler #0"
    FOR i=0 TO 9
      IF LPEEK(VARPTR(kanone$(0))+16*i)<>-1
        PRINT i,LPEEK(VARPTR(kanone$(0))+16*i),LPEEK(VARPTR(kanone$(0))+16*i+4)
      ENDIF
    NEXT i
    PRINT "Kanonen für Spieler #1"
    FOR i=0 TO 9
      IF LPEEK(VARPTR(kanone$(1))+16*i)<>-1
        PRINT i,LPEEK(VARPTR(kanone$(1))+16*i),LPEEK(VARPTR(kanone$(1))+16*i+4)
      ENDIF
    NEXT i
  ENDIF
RETURN

' /***************************** Zeichenroutine ********************************/
PROCEDURE draw(x,y,a%)
  ' short x,y,*a;
  DIM px(100),py(100)
  LOCAL i,fil,f1,f2
  fil=1

  @hide
  ' vsf_interior( handle,2 );
  ' vsf_style( handle,9 );
  COLOR schwarz
  f1=2
  f2=9
  WHILE LPEEK(a%)<>-1
    IF LPEEK(a%)=-2
      ADD a%,4
      f1=LPEEK(a%)
      ADD a%,4
      f2=LPEEK(a%)
    ELSE IF LPEEK(a%)=-4
      fil=not fil
    ELSE
      i=0
      WHILE LPEEK(a%)>-1
        px(i)=x+LPEEK(a%)*f
        ADD a%,4
        py(i)=y-LPEEK(a%)
        ADD a%,4
        INC i
      WEND
      IF fil
        px(i)=px(0)
        py(i)=py(0)
        '  inc i
        DEFFILL ,f1,f2
        POLYFILL i,px(),py()
        DEFFILL ,0
        POLYLINE i,px(),py()
        PLOT px(i-1),py(i-1)
      ELSE
        POLYLINE i,px(),py()
        PLOT px(0),py(0)
        PLOT px(i-1),py(i-1)
      ENDIF
    ENDIF
    ADD a%,4
  WEND
  @show
RETURN

PROCEDURE init_ka(n,k,xr)    ! Kanone k setzen
  LOCAL x,y,adr%,burg_adr%
  '  print "Beleg Kanone #";k;" fuer n=";n;" f=";f;" x=";xr
  burg_adr%=VARPTR(burgen$(bur(n)))
  x=xr+LPEEK(burg_adr%+4*(1+k*2))*f
  y=burg_y(n AND 1)-LPEEK(burg_adr%+4*(2+k*2))
  @draw(x,y,varptr(kanon$))
  IF n<2
    adr%=VARPTR(kanone$(n))
    LPOKE adr%+k*16+0,x-20*n
    LPOKE adr%+k*16+4,y
    LPOKE adr%+k*16+8,45    ! Winkel
    LPOKE adr%+k*16+12,12   ! Pulver
  ENDIF
RETURN
PROCEDURE drw_all  ! Geld, Pulver und Kugeln zeichnen */
  @drw_gpk(0)
  @drw_gpk(1)
  @drw_gpk(2)
RETURN
PROCEDURE drw_gpk(w)
  LOCAL i,z,x,y,xr,yr,a%,xp,yp,m,cx,cy,cw,ch,burg_adr%
  m=n and 1
  IF w=0
    a%=VARPTR(sack$)
    xp=7
    yp=10
    i=(geld(m)+149)/150
  ELSE IF w=1
    a%=VARPTR(fass$)
    xp=9
    yp=9
    i=(pu(m)+29)/30
  ELSE IF w=2
    a%=VARPTR(kuge$)
    xp=6
    yp=6
    i=kugeln(m)
  ELSE
    PRINT "something is wrong"
    END
  ENDIF
  burg_adr%=VARPTR(burgen$(bur(m)))
  w=w*2
  IF n AND 2
    xr=burg_x(m)
  ELSE
    xr=(bw-1)*m
  ENDIF
  xr=xr+f*LPEEK(burg_adr%+4*(25+w))
  yr=burg_y(m)-LPEEK(burg_adr%+4*(26+w))
  @hide
  cx=xr-LPEEK(burg_adr%+4*(31+w))*m-abs(n=0)
  cy=yr-LPEEK(burg_adr%+4*(32+w))
  cw=LPEEK(burg_adr%+4*(31+w))+1
  ch=LPEEK(burg_adr%+4*(32+w))+1
  @clr(cx,cy,cw,ch)
  COLOR schwarz
  CLR y,z
  WHILE i>0 AND y<LPEEK(burg_adr%+4*(32+w))
    CLR x
    WHILE i>0 AND x<LPEEK(burg_adr%+4*(31+w))
      @draw(xr+f*x,yr-y,a%)
      ADD x,xp
      INC z
      DEC i
    WEND
    ADD y,yp
  WEND
  @show
  IF i>0     !  Maximalbetrag �berschritten ? */
    IF w=0
      geld(m)=z*150
    ELSE IF w=2
      pu(m)=z*30
    ELSE IF w=4
      kugeln(m)=z
    ENDIF
  ENDIF
RETURN

' /********************* Neues Spiel: Auswahl der Burgen ***********************/
FUNCTION bur_obj
  LOCAL i,ob0,ob1,oy0,oy1,x,y,w,h,t1$,t2$
  DIM ol(8)
  a_brg=RSRC_GADDR(0,3)
  oy0=burg_y(0)
  oy1=burg_y(1)
  ob0=bur(0)
  ob1=bur(1)

  t1$=l_nam$+CHR$(0)+SPACE$(32)
  t2$=r_nam$+CHR$(0)+SPACE$(32)
  LPOKE a_brg+24*1+12,VARPTR(t1$)
  LPOKE a_brg+24*2+12,VARPTR(t2$)

  ~form_center(a_brg,fx,fy,fw,fh)

  w=dpeek(a_brg+24*4+20)
  h=dpeek(a_brg+24*4+22)
  ~objc_offset(a_brg,4,x,y)
  burg_x(0)=x
  burg_y(0)=y+h
  ~objc_offset(a_brg,5,x,y)
  burg_x(1)=x+w
  burg_y(1)=y+h

  ~form_dial(0,70,20,30,20,fx,fy,fw,fh)
  ~form_dial(1,70,20,30,20,fx,fy,fw,fh)
  ~objc_draw(a_brg,0,5,0,0,0,0)
  ol(0)=geld(0)
  ol(1)=geld(1)
  ol(2)=pu(0)
  ol(3)=pu(1) ! /* Sichern der alten*/
  ol(4)=kugeln(0)
  ol(5)=kugeln(1)
  ol(6)=volk(0)
  ol(7)=volk(1) ! /* Werte */
  @burg(2)
  @burg(3)
  DO
    geld(0)=9999
    geld(1)=9999
    pu(0)=9999
    pu(1)=9999
    kugeln(0)=9999
    kugeln(1)=9999
    i=FORM_DO(a_brg)
    DPOKE a_brg+24*i+10,0
    IF i=3 OR i=6
      bur(0)=(bur(0)+1-2*abs(i=3)+b_anz) mod b_anz
      @clr(burg_x(0),y,w,h)
      @burg(2)
    ENDIF
    IF i=7 OR i=8
      bur(1)=(bur(1)+1-2*abs(i=7)+b_anz) mod b_anz
      @clr(burg_x(1)-w,y,w,h)
      @burg(3)
    ENDIF
    EXIT IF i=9 OR i=10
  LOOP
  geld(0)=ol(0)
  geld(1)=ol(1)
  pu(0)=ol(2)
  pu(1)=ol(3)
  kugeln(0)=ol(4)
  kugeln(1)=ol(5)
  volk(0)=ol(6)
  volk(1)=ol(7)

  ~form_dial(2,70,20,30,20,fx,fy,fw,fh)
  ~form_dial(3,70,20,30,20,fx,fy,fw,fh)

  f=1-2*(n AND 1)
  burg_y(0)=oy0
  burg_y(1)=oy1
  IF i=9
    bur(0)=ob0
    bur(1)=ob1
  ENDIF
  RETURN abs(i=10)
ENDFUNCTION

PROCEDURE koenig
  '  char a[300],*s[20]
  DIM s$(20)
  LOCAL k,t,j,i,kanone_adr%
  IF n=0
    kanone_adr%=VARPTR(kanone$(0))
    foerderturm_adr%=VARPTR(foerderturm$(0))
  ELSE
    kanone_adr%=VARPTR(kanone$(1))
    foerderturm_adr%=VARPTR(foerderturm$(1))
  ENDIF
  CLR k,t
  ' zähle die Förtertürme und Kanonen
  FOR j=k=0 TO 9
    ADD k,abs(LPEEK(kanone_adr%+16*j+0)>-1)
  NEXT j
  FOR j=k=0 TO 4
    ADD t,abs(LPEEK(foerderturm_adr%+16*j+0)>-1)
  NEXT j

  IF random(20)=0 OR (kn(n) AND 16)=16 OR (kn(n) AND 15)>9
    ~form_alert(1,"[0][Der König hat keine Lust,   |dich zu sprechen.][Schade]")
  ELSE
    IF kn(n)>2559
      POKE varptr(kn8$)+46,48+kn(n)/2560
    ELSE
      POKE varptr(kn8$)+46,32
    ENDIF
    POKE varptr(kn8$)+47,48+(kn(n) MOD 2560)/256
    j=Random(65535)
    i=2
    IF (j AND 1)=1
      s$(0)=kn0$
    ELSE
      s$(0)=kn10$
    ENDIF
    IF (j AND 2)=2
      s$(1)=kn12$
    ELSE
      s$(1)=kn6$
    ENDIF
    IF (j AND 4)=4
      s$(i)=kn14$
      INC i
    ENDIF
    IF geld(n)>price(1) AND t<3
      s$(i)=kn5$
      INC i
    ENDIF
    IF steuersatz(n)>40 AND volk(n)<LPEEK(burg_adr%+4*40) OR steuersatz(n)>70
      s$(i)=kn3$
      INC i
    ENDIF
    IF wx(n)<0
      s$(i)=kn9$
      INC i
    ENDIF
    IF geld(n)>LPEEK(burg_adr%+4*37) AND volk(n)>LPEEK(burg_adr%+4*40) AND k>1
      IF t<2
        s$(1)=kn1$
      ELSE
        s$(1)=kn2$
      ENDIF
    ENDIF
    IF t>2
      s$(i)=kn2$
      INC i
    ENDIF
    IF k<1 AND geld(n)<price(2)
      s$(i)=kn4$
      INC i
    ENDIF
    IF (kn(n) AND 15)>4
      s$(i)=kn7$
      INC i
      s$(i)=kn7$
      INC i
      s$(i)=kn7$
      INC i
    ENDIF
    IF (kn(n) AND 15)>6 OR (not Random(7)) AND kn(n)>2048
      s$(i)=kn8$
      INC i
      s$(i)=kn8$
      INC i
    ENDIF
    IF zug<4
      s$(1)=kn11$
      s$(2)=kn11$
      s$(3)=kn11$
      i=4
    ENDIF
    DO
      j=RANDOM(i)
      IF s$(j)=ltz$(n)
        s$(i)=kn13$
        INC i
        s$(i)=kn13$
        INC i
      ENDIF
      EXIT IF s$(j)<>ltz$(n)
    LOOP
    ltz$(n)=s$(j)
    ~form_alert(1,kna$+ltz$(n)+kne$)
  ENDIF
  kn(n)=kn(n) or 16
  kn(n)=kn(n)+256
  IF (not kn(n)) AND 15
    kn(n)=kn(n)+1
  ENDIF
RETURN

FUNCTION drin(xk,yk,w,h,r,x,y) ! Test, ob Koord. innerhalb eines Rechteckes */
  IF n
    xk=bw-1-xk-w
  ENDIF
  '  COLOR blau
  '  box xk,burg_y(n)-yk,xk+w,burg_y(n)-yk-h
  '  circle x,y,r
  '  SHOWPAGE
  '  PAUSE 1
  RETURN abs(x>xk-r AND x<xk+w+r AND y<burg_y(n)-yk+r AND y>burg_y(n)-yk-h-r)
ENDFUNCTION

' /******************************** Markt **************************************/
PROCEDURE markt
  LOCAL a,k,ko,t,kanone_adr%,foerderturm_adr%,marktdialog_adr%
  marktdialog_adr%=RSRC_GADDR(0,6)
  ~form_center(marktdialog_adr%,fx,fy,fw,fh)
  ~form_dial(0,mx,my,30,20,fx,fy,fw,fh)
  ~form_dial(1,mx,my,30,20,fx,fy,fw,fh)
  kanone_adr%=VARPTR(kanone$(n))
  foerderturm_adr%=VARPTR(foerderturm$(n))
  FOR a=0 TO 5
    @markt_zahl(16+a,price(a))
  NEXT a
  DO
    CLR t,ko
    FOR k=0 TO 4
      ADD t,abs(LPEEK(foerderturm_adr%+k*16+0)>-1)
    NEXT k
    FOR k=0 TO 9
      ADD ko,abs(LPEEK(kanone_adr%+k*16+0)>-1)
    NEXT k
    FOR k=0 TO 9
      IF LPEEK(burg_adr%+4*(1+k*2))>-1 AND LPEEK(kanone_adr%+16*k+0)=-1
        FOR a=1 TO 9
          EXIT IF POINT((bw-1)*n+(LPEEK(burg_adr%+4*(1+k*2))+5+a)*f,burg_y(n)-LPEEK(burg_adr%+4*(2+k*2))-a)<>weiss
        NEXT a
        EXIT IF a>9
      ENDIF
    NEXT k
    @markt_zahl(3,geld(n))
    @markt_zahl(4,t)
    @markt_zahl(5,ko)
    @markt_zahl(6,abs(wx(n)>-1))
    @markt_zahl(7,pu(n))
    @markt_zahl(8,kugeln(n))
    @markt_zahl(9,volk(n))
    @markt_zahl(10,steuersatz(n))
    FOR a=0 TO 5
      DPOKE (marktdialog_adr%+24*(16+a)+10),8*abs(geld(n)<price(a) OR (anbauen_erlaubt=0 AND a=0) OR \
      (a=1 AND (LPEEK(burg_adr%+4*0)+t*30>265 OR t>4)) or (a=2 and k>9) or (a=3 and wx(n)>-1))
    NEXT a
    ~objc_draw(marktdialog_adr%,0,5,0,0,0,0)

    a=FORM_DO(marktdialog_adr%)
    IF a>=0
      DPOKE marktdialog_adr%+24*a+10,0
    ENDIF
    IF a=11
      steuersatz(n)=steuersatz(n)-2*abs(steuersatz(n)>0)
    ELSE IF a=12
      steuersatz(n)=steuersatz(n)+2*abs(steuersatz(n)<100)
    ELSE IF a<>13 AND a-16>-1 AND a-16<6
      geld(n)=geld(n)-price(a-16)
      IF a<20
        ~form_dial(2,mx,my,30,20,fx,fy,fw,fh)
        ~form_dial(3,mx,my,30,20,fx,fy,fw,fh)
        @drw_all
        IF a=16
          @anbau
        ELSE IF a=17
          @fturm
        ELSE IF a=18
          @init_ka(n,k,(bw-1)*n)
        ELSE IF a=19
          wx(n)=(bw-1)*n+f*LPEEK(burg_adr%+4*23)
          wy(n)=burg_y(n)-LPEEK(burg_adr%+4*24)
          @werdran(1)
        ENDIF
        ~form_dial(0,mx,my,30,20,fx,fy,fw,fh)
        ~form_dial(1,mx,my,30,20,fx,fy,fw,fh)
      ELSE
        pu(n)=pu(n)+30*abs(a=20)
        kugeln(n)=kugeln(n)+2*abs(a=21)
        @drw_gpk(a-19)
        @drw_gpk(0)
      ENDIF
    ENDIF
    EXIT IF a=13
  LOOP
  @drw_all
  ~form_dial(2,mx,my,30,20,fx,fy,fw,fh)
  ~form_dial(3,mx,my,30,20,fx,fy,fw,fh)
RETURN

' /* Anbauen */
PROCEDURE anbau
  LOCAL s,mx,my,bt,mxmin,mxmax
  GRAPHMODE 1

  IF n=0
    mxmin=bx
    mxmax=LPEEK(burg_adr%)+15
  ELSE
    mxmin=bx+bw-LPEEK(burg_adr%)-16
    mxmax=bx+bw
  ENDIF

  COLOR schwarz
  DEFFILL ,2,9
  ~form_dial(0,80,by+bh-25,24,100,fx,fy,fw,fh)
  ~form_dial(1,80,by+bh-25,24,100,fx,fy,fw,fh)
  TEXT 80,by+bh-25," Anbauen: "
  TEXT 220,by+bh-5," Verbleibende Steine: 20 "
  s=20
  DEFMOUSE 1
  DO
    MOUSE mx,my,bt
    IF bt AND mx>mxmin AND mx<mxmax AND my>155
      IF not (POINT(mx,my)=schwarz OR POINT(mx+1,my+1)=schwarz OR POINT(mx-1,my-1)) \
        AND (POINT(mx+3,my-1)=schwarz OR POINT(mx+3,my+1)=schwarz OR \
        POINT(mx-3,my-1)=schwarz or POINT(mx-3,my+1)=schwarz or \
        POINT(mx+1,my+2)=schwarz or POINT(mx-1,my+2)=schwarz or \
        POINT(mx+1,my-2)=schwarz or POINT(mx-1,my-2)=schwarz)
        PBOX mx-2,my-1,mx+2,my+1
        DEC s
        PAUSE 0.2
        TEXT 396,by+bh-5,STR$(s,2,2)
      ENDIF
    ENDIF
    SHOWPAGE
    EXIT IF s<=0 OR bt>=2
  LOOP
  ~form_dial(2,80,by+bh-25,25,100,fx,fy,fw,fh)
  ~form_dial(3,80,by+bh-25,25,100,fx,fy,fw,fh)
  DEFMOUSE 0
RETURN

PROCEDURE markt_zahl(nr,wert) ! /* 5-stellige Zahl, rechtsb�ndig, ohne f�hrende Nullen */
  LOCAL i,a,b,adr%,marktdialog_adr%
  marktdialog_adr%=RSRC_GADDR(0,6)
  adr%=LPEEK(marktdialog_adr%+24*nr+12)+11
  CLR b,i
  a=10000
  wert=MAX(wert,0)
  WHILE i<5
    POKE adr%,48+(wert DIV a)-16*abs(wert<a AND i<4 AND (b=0))
    ' print chr$(48+(wert div a)-16*abs(wert<a and i<4 and (not b)));
    ' print chr$(48+(wert div a));
    b=b or (wert div a)
    '     print " ";a,wert,wert div a
    wert=wert mod a
    INC adr%
    INC i
    a=a div 10
  WEND
RETURN
FUNCTION comp  !/* F�hrt einen Zug des Computers durch */
  LOCAL i,t,wi,x,x2,y,vx2,vx,vy,wd,kanone_adr%,foerderturm_adr%,burg_adr%
  CLR vy,x2

  kanone_adr%=VARPTR(kanone$(n))
  foerderturm_adr%=VARPTR(foerderturm$(n))
  burg_adr%=VARPTR(burgen$(bur(n)))

  steuersatz(n)=16     ! setze festen (niedrigen) Steuersatz
  FOR i=0 TO 9
    EXIT IF LPEEK(kanone_adr%+16*i+0)>-1
  NEXT i
  ' print "computer spielt, Seite=";n
  ' print "Kanone #";i
  IF pu(n)<20 AND geld(n)>=price(4)
    pu(n)=pu(n)+30
    geld(n)=geld(n)-price(4) !  /* Pulv. kaufen*/
    ' print "Pulver gekauft."
  ENDIF
  IF kugeln(n)=0 AND geld(n)>=price(5)
    kugeln(n)=kugeln(n)+2
    geld(n)=geld(n)-price(5) !  /* Kug.  kaufen*/
    ' print "Kugeln gekauft."
  ENDIF
  IF i>9 AND geld(n)>=price(2)
    i=0
    @init_ka(n,i,(bw-1)*n)
    geld(n)=geld(n)-price(2) ! /* Ka. kauf.*/
    ' print "Kanone gekauft."
  ENDIF
  IF (LPEEK(foerderturm_adr%+16*0)<0 OR LPEEK(foerderturm_adr%+16*1)<0 OR LPEEK(foerderturm_adr%+16*2)<0) AND geld(n)>=price(1) AND cw(n)>2
    geld(n)=geld(n)-price(1)
    @fturm    !  /* F�rderturm kaufen */
    ' print "Foerderturm gekauft."
  ENDIF
  @drw_all

  IF i>9 OR pu(n)<20 OR kugeln(n)=0
    ' print "kann nix tun."
    RETURN -1
  ENDIF

  '  /* Jetzt kommen die Berechnungen f�r den Schuss: */

  bh=VARPTR(burgen$(bur(abs(n=0))))
  ' Waehle per Zufall eine Kanone aus
  DO
    i=Random(10)
    EXIT IF LPEEK(kanone_adr%+i*16)<>-1
  LOOP
  ' print "Ziele auf Gegner mit Kanone #";i
  zx=(bw-1)*abs(n=0)-f*Random(LPEEK(bh))
  zy=burg_y(abs(n=0))
  ' COLOR blau
  ' line zx,zy-5,zx,zy+5
  'line zx-5,zy,zx+5,zy
  ' SHOWPAGE
  ' print "Ziel: ";zx,zy
  ' print "Strategie ";cw(n)
  t=Random(100)
  IF cw(n)
    @z_kn
  ENDIF
  ' COLOR blau
  ' line zx,zy-5,zx,zy+5
  ' line zx-5,zy,zx+5,zy
  ' SHOWPAGE
  ' print "Ziel Koenig: ";zx,zy
  ' Die verschiedenen Strategien */
  IF cw(n)=1
    IF t<30
      @z_ge
    ENDIF
    IF t>60
      @z_pk
    ENDIF
  ELSE IF cw(n)=2
    @z_ka()
    IF t<90
      @z_ge
    ENDIF
  ELSE IF cw(n)=3
    IF t<50
      @z_ka
    ELSE IF t<70
      @z_ge
    ELSE IF t<90
      @z_pk
    ENDIF
    IF Random(3)=0
      @z_ft
    ENDIF
  ELSE IF cw(n)=4
    @z_ka
  ELSE IF cw(n)=5
    @z_ka
    IF t<90
      @z_ft
    ENDIF
  ENDIF
  COLOR blau
  LINE zx,zy-5,zx,zy+5
  LINE zx-5,zy,zx+5,zy
  SHOWPAGE
  ' print "Ziel endgültig: ";zx,zy

  y=LPEEK(kanone_adr%+16*i+4)-10-zy
  t=49-cx(n)*16  !  Berechnen der Distanz */
  x=LPEEK(kanone_adr%+16*i+0)+9+8*f-zx-t/2+Random(t)
  IF x<0
    x=-x
  ENDIF
  IF n
    wd=-wind
  ELSE
    wd=wind
  ENDIF
  x2=-1

  vx=0.5
  WHILE vx<7 AND (x2<x OR vy/3.5>vx)  ! Zeitberechnung bei */
    ' verschiedenen X-Geschwindigkeiten */
    ADD vx,0.4
    CLR t,x2
    vx2=vx
    ' print t,x2,vx2,vy,x,wd
    WHILE vx2>0 AND x2<x AND t<10000
      ADD x2,vx2
      ADD vx2,(wd/2-vx2)/5000
      INC t
    WEND
    vy=y/t+0.5*0.02*t
  WEND
  ' print "Zeitschritte: t=";t
  IF n
    vx=-vx
  ENDIF
  vvx=vx
  vvy=vy  !/* Werte für Schuss übergeben */
  IF vx<0
    vx=-vx
  ENDIF
  wi=atan2(vy,vx)
  ' print "Schiesse: winkel=";wi," vx=";vx
  LPOKE kanone_adr%+i*16+8,wi*57.296
  LPOKE kanone_adr%+i*16+12,INT(4.0*vx/cos(wi)-1.6)
  RETURN i
ENDFUNCTION
' /********************* Routinen zur Zielerkennung: ***************************/
' /* Die Routinen berechnen die Zielkoordinaten f�r den K�nig, Kanonen, Geld...*/
' liefere Position des Koenigs
PROCEDURE z_kn
  zx=abs(n=0)*(bw-1)-f*(LPEEK(bh+4*21)+15)
  zy=burg_y(abs(n=0))-LPEEK(bh+4*22)-5
RETURN
' Position einer Kanone
PROCEDURE z_ka
  LOCAL i,kanone_adr%
  kanone_adr%=VARPTR(kanone$(abs(n=0)))
  FOR i=0 TO 9
    EXIT IF LPEEK(kanone_adr%+16*i)>-1
  NEXT i
  IF i<10
    DO
      i=Random(10)
      EXIT IF LPEEK(kanone_adr%+16*i)<>-1
    LOOP
    zx=LPEEK(kanone_adr%+16*i+0)+10
    zy=LPEEK(kanone_adr%+16*i+4)
  ENDIF
RETURN
' Position eines Foeerderturms
PROCEDURE z_ft
  LOCAL i,foerderturm_adr%
  foerderturm_adr%=VARPTR(foerderturm$(abs(n=0)))
  FOR i=0 TO 4
    EXIT IF LPEEK(foerderturm_adr%+16*i)>-1
  NEXT i
  IF i<5
    zx=LPEEK(foerderturm_adr%+16*i+0)-15*f
    zy=LPEEK(foerderturm_adr%+16*i+4)-10
  ENDIF
RETURN
' Position des Geldspeichers
PROCEDURE z_ge
  IF geld(abs(n=0))>100
    zx=abs(n=0)*(bw-1)-f*(LPEEK(bh+4*25)+LPEEK(bh+4*31)/2)
    zy=burg_y(abs(n=0))-LPEEK(bh+4*26)
  ENDIF
RETURN
' Position des Pulvervorrats
PROCEDURE z_pk
  LOCAL i
  IF kugeln(abs(n=0)) OR pu(abs(n=0))>19
    i=Random(2)
    zx=abs(n=0)*(bw-1)-f*(LPEEK(bh+4*(27+i))+LPEEK(bh+4*(33+i))/2)
    zy=burg_y(abs(n=0))-LPEEK(bh+4*(28+i))
  ENDIF
RETURN

' /********************* Von Markt aufzurufende Routinen ***********************/
PROCEDURE fturm    !  Förderturm bauen */
  LOCAL x,y,yy,i,t,foerderturm_adr%,burg_adr%
  foerderturm_adr%=VARPTR(foerderturm$(n))
  burg_adr%=VARPTR(burgen$(bur(n)))
  @hide
  FOR t=0 TO 4
    EXIT IF LPEEK(foerderturm_adr%+16*t)=-1
  NEXT t
  x=(bw-1)*n+f*(LPEEK(burg_adr%)+20+30*t)
  y=by+bh-20
  DO
    WHILE POINT(x,y)<>weiss AND POINT(x+29*f,y)<>weiss
      DEC y
    WEND
    yy=y
    FOR i=0 TO 39
      EXIT IF POINT(x,y)<>weiss AND POINT(x+29*f,y)<>weiss
      DEC y
    NEXT i
    EXIT IF i>=40
  LOOP
  y=yy
  @clr(x-29*n,y-70,30,70)
  COLOR schwarz
  LPOKE foerderturm_adr%+16*t+0,x
  LPOKE foerderturm_adr%+16*t+4,y
  @draw(x,y,varptr(turm$))
  @show
RETURN
PROCEDURE dosound(f$)
  IF dosound=1
    IF EXIST(sounddir$+f$)
      PLAYSOUNDFILE sounddir$+f$
    ENDIF
  ENDIF
RETURN
' /********************************* Ein Schuss ********************************/
' k= Nr. der Kanone */
PROCEDURE schuss(k)
  LOCAL pulver,winkel,kanone_adr%,x,y,vx,vy,vvx,vvy
  LOCAL ox,oy,v,c,a,j

  kanone_adr%=VARPTR(kanone$(n))

  pulver=LPEEK(kanone_adr%+16*k+12)
  winkel=LPEEK(kanone_adr%+16*k+8)

  @hide
  pu(n)=pu(n)-pulver
  kugeln(n)=kugeln(n)-1
  @drw_gpk(1)   ! pulver neuzeichnen
  @drw_gpk(2)   ! kugeln neuzeichnen
  SHOWPAGE
  PAUSE 0.1
  x=LPEEK(kanone_adr%+16*k+0)+9+8*f
  y=LPEEK(kanone_adr%+16*k+4)-10
  c=20 ! vorlauf ...
  vx=(0.4+0.25*pulver*cos(winkel/180*pi))*f
  vy=(0.4+0.25*pulver*sin(winkel/180*pi))

  IF spiel_modus AND (2-n)
    vx=vvx
    vy=vvy
  ENDIF
  COLOR schwarz
  ' graphmode 3

  @baller(0)

  ~@kugel(x,y)
  SHOWPAGE
  v=1
  ' print "Abschuss von ",x,y
  ' print "Geschw:      ",vx,vy
  PAUSE 0.8
  @dosound("bumm.ogg")
  PAUSE 0.2
  WHILE x>3 AND x<bw-3 AND y<by+bh-4 AND (v<>0 OR c>0)  ! Flugschleife */
    ox=x
    oy=y
    ADD x,vx
    SUB y,vy
    SUB vy,0.02
    ADD vx,(wind/2-vx)/5000
    COLOR weiss
    ~@kugel(ox,oy)
    COLOR schwarz
    ' print "p:",x,y,v,c
    SHOWPAGE
    IF y>by+10
      v=((POINT(x,y)=weiss) AND (POINT(x-1,y+1)=weiss) AND (POINT(x+1,y+2)=weiss))
      ~@kugel(x,y)
    ENDIF
    SHOWPAGE
    PAUSE 0.01
    a=1000+2*y
    a=MAX(a,30)
    '  Giaccess( 10,137 );
    '  Giaccess( 244,135 );
    '  Giaccess( a&255,130 );
    '  Giaccess( a>>8,131 );
    IF dosound=2
      SOUND ,a
    ENDIF
    IF c>0
      DEC c
    ENDIF
  WEND

  COLOR weiss
  oldn=n
  IF not v
    FOR c=0 TO 3    ! Einschlag der Kugel
      IF ox>6 AND ox<bw-6
        PCIRCLE ox,oy,5
      ENDIF
      @baller(22+c*3)
      SHOWPAGE
      PAUSE 0.01
      @dosound("einschlag.ogg")
      PRINT at(1,1);
      FOR n=0 TO 1  ! Treffer ?
        burg_adr%=VARPTR(burgen$(bur(n)))
        IF Random(2) AND ((n>0 AND x>(bw-1)-LPEEK(burg_adr%)) OR (n=0 AND x<LPEEK(burg_adr%))) AND volk(n)>0
          DEC volk(n)
          PRINT "Volk wird kleiner."
        ENDIF
        f=1-2*(n AND 1)
        IF n=0
          kanone_adr%=VARPTR(kanone$(0))
          foerderturm_adr%=VARPTR(foerderturm$(0))
        ELSE
          kanone_adr%=VARPTR(kanone$(1))
          foerderturm_adr%=VARPTR(foerderturm$(1))
        ENDIF
        FOR j=0 TO 9
          IF LPEEK(kanone_adr%+16*j+0)<ox+2 AND \
            LPEEK(KANONE_ADR%+16*J+0)+22>OX and \
            LPEEK(KANONE_ADR%+16*J+0)>-1 and \
            LPEEK(KANONE_ADR%+16*J+4)-16<OY and LPEEK(kanone_adr%+16*j+4)>oy
            a=LPEEK(kanone_adr%+16*j+0)
            v=LPEEK(kanone_adr%+16*j+4)-12
            @clr(a,v,20,13)
            @dosound("die.ogg")
            @expls(a+10,v+6,13,8,50)
            COLOR weiss
            LPOKE kanone_adr%+16*j+0,-1
          ENDIF
        NEXT j
        FOR j=0 TO 4
          a=(bw-1)*n+f*LPEEK(foerderturm_adr%+16*j+0)
          v=burg_y(n)-LPEEK(foerderturm_adr%+16*j+4)
          IF LPEEK(foerderturm_adr%+16*j+0)>-1 AND @drin(a,v,30,37,2,ox,oy)
            IF @drin(a,v,30,37,-2,ox,oy)
              a=LPEEK(foerderturm_adr%+16*j+0)-29*n
              v=LPEEK(foerderturm_adr%+16*j+4)-40
              @dosound("flail.ogg")
              @expls(a+15,v+20,15,20,120)
              @clr(a,v,30,40)
              LPOKE foerderturm_adr%+16*j+0,-1
            ENDIF
            c=4
          ENDIF
        NEXT j
        IF ox>wx(n)-11 AND ox<wx(n)+11 AND oy>wy(n)-16 AND oy<wy(n) AND wx(n)>-1
          ' Windfahne weg
          @dosound("flagweg.ogg")
          @clr(wx(n)-10,wy(n)-15,20,15)
          wx(n)=-1
        ENDIF
        IF @drin(LPEEK(burg_adr%+4*21),LPEEK(burg_adr%+4*22),30,25,0,ox,oy)
          ' Koenig wurde getroffen
          end=n+17
          a=(bw-1)*n+f*(LPEEK(burg_adr%+4*21)+15)
          v=burg_y(n)-LPEEK(burg_adr%+4*22)-12
          @dosound("explosion.ogg")
          @expls(a,v,17,17,40)
          PCIRCLE a,v,17
          @expls(a,v,17,17,200)
          c=4
        ENDIF
        FOR j=0 TO 4 STEP 2
          a=LPEEK(burg_adr%+4*(31+j))
          v=LPEEK(burg_adr%+4*(32+j))
          EXIT IF @drin(LPEEK(burg_adr%+4*(25+j)),LPEEK(burg_adr%+4*(26+j)),a,v,3,ox,oy)=1
        NEXT j
        IF j=0
          SUB geld(n),MIN(geld(n),200)
          PRINT "Geld weniger"
        ELSE IF j=2
          pu(n)=0
          PRINT "Pulver weg"
        ELSE IF j=4
          SUB kugeln(n),MIN(kugeln(n),2)
          PRINT "Kugeln weniger"
        ENDIF
        ' print "j=",j
        IF j<6
          @dosound("explosion.ogg")
          @expls((bw-1)*n+f*(LPEEK(burg_adr%+4*(25+j))+a/2),burg_y(n)-LPEEK(burg_adr%+4*(26+j))-v/2,a/2,v/2,60+100*abs(j=2))
          @drw_gpk(j/2)
          c=4
        ENDIF
      NEXT n
      ADD ox,vx
      SUB oy,vy
    NEXT c
  ENDIF
  n=oldn
  f=1-2*(n AND 1)
  ' Giaccess( 0,137 );
  IF dosound=2
    SOUND ,0
  ENDIF
  @show
RETURN
' /****************************** Explosion ************************************/
PROCEDURE expls(x,y,w,h,d)
  LOCAL i,j,od
  '  graphmode 3
  DIM px(d+1),py(d+1)
  px(d)=x
  py(d)=y
  od=d
  COLOR schwarz
  WHILE d>0
    DEC d
    px(d)=x-w+w*Random(512)/256
    py(d)=y-h+h*Random(512)/256

    LINE px(d+1),py(d+1),px(d),py(d)
    SHOWPAGE
    PAUSE 0.005
    @baller((d AND 31) MOD 31)
  WEND
  ' graphmode 1
  COLOR weiss
  FOR i=0 TO od-1
    LINE px(i+1),py(i+1),px(i),py(i)
    SHOWPAGE
    PAUSE 0.005
  NEXT i
RETURN

' Draw the ball
FUNCTION kugel(x,y)
  IF x<3 OR x>bw-3 OR y<by+7
    RETURN 0
  ENDIF
  LINE x-2,y-2,x+1,y-2
  LINE x-3,y-1,x+2,y-1
  LINE x-3,y,x+2,y
  LINE x-3,y+1,x+2,y+1
  LINE x-2,y+2,x+1,y+2
  RETURN 1
ENDFUNCTION
PROCEDURE baller(r)
  '   static char s_bal[]={ 0,0,1,15,6,31,9,0,11,0,12,50,13,0,7,192,8,16,255,0 };
  '  s_bal[5]=r;
  '  Dosound( s_bal );
RETURN
PROCEDURE computerwahl
  LOCAL i,ret,computerwahl_adr%
  computerwahl_adr%=RSRC_GADDR(0,8)
  ~FORM_CENTER(computerwahl_adr%,x,y,w,h)
  ~FORM_DIAL(0,x,y,w,h,x,y,w,h)
  ~FORM_DIAL(1,x,y,w,h,x,y,w,h)
  ~OBJC_DRAW(computerwahl_adr%,0,-1,0,0,0,0)
  ret=FORM_DO(computerwahl_adr%)
  IF ret>0 AND ret<44
    DPOKE computerwahl_adr%+ret*24+10,0    ! Gewaehlten Knopf wieder deselectieren
  ENDIF
  ~FORM_DIAL(2,x,y,w,h,x,y,w,h)
  ~FORM_DIAL(3,x,y,w,h,x,y,w,h)
  FOR i=0 TO 6
    EXIT IF DPEEK(computerwahl_adr%+24*(3+i)+10)<>0
  NEXT i
  cw(0)=i
  FOR i=0 TO 6
    EXIT IF DPEEK(computerwahl_adr%+24*(11+i)+10)<>0
  NEXT i
  cw(1)=i
  FOR i=0 TO 3
    EXIT IF DPEEK(computerwahl_adr%+24*(19+i)+10)<>0
  NEXT i
  cx(0)=i
  FOR i=0 TO 3
    EXIT IF DPEEK(computerwahl_adr%+24*(24+i)+10)<>0
  NEXT i
  cx(1)=i
RETURN

' /********** Berechnen von Bev�lkerungszuwachs usw. nach jedem Zug ************/
PROCEDURE rechnen
  LOCAL j,foerderturm_adr%
  price_minimum()=[98,347,302,102,30,29] ! Preisgrenzen
  price_maximum()=[302,707,498,200,89,91]
  price_sigma()=[10,50,50,20,10,10]              ! max. Preisschwankung

  foerderturm_adr%=VARPTR(foerderturm$(n))

  ' Ertrag durch Steuereinnahmen
  j=steuersatz(n)
  IF j>65
    geld(n)=geld(n)+volk(n)*(130-j)/(150-RANDOM(50))
  ELSE
    geld(n)=geld(n)+volk(n)*j/(150-RANDOM(50))
  ENDIF
  volk(n)=volk(n)*(95+RANDOM(11))/100+(21-j+RANDOM(9))*(8+RANDOM(5))/20
  IF volk(n)<0 ! Wenn alle weggelaufen sind, ist das Spiel beendet.
    volk(n)=0
    end=n+49
  ENDIF
  ' Ertrag aus Fördertürmen
  FOR j=0 TO 4
    geld(n)=geld(n)+(40+RANDOM(32))*ABS(LPEEK(foerderturm_adr%+16*j)>-1)
  NEXT j
  ' Neue Marktpreise
  FOR j=0 TO 5
    price(j)=price(j)+price_sigma(j)*RANDOM(99)/98-price_sigma(j)/2
    price(j)=MAX(price(j),price_minimum(j))
    price(j)=MIN(price(j),price_maximum(j))
  NEXT j
  @drw_gpk(0)  ! Geld neuzeichnen
RETURN

' ***************** Grafikdaten für Trohn, Kanone... **************************/
trohn:
DATA -2,2,2, 8,3,5,12,8,12,12,16,12,17,14,15
DATA 15,15,17,17,17,16,21,11,24,11,21,3,-9,-4,3,0,5,2,24,2,26,0,-9
DATA 19,4,18,9,-9,10,4,11,9,-9,14,4,15,4,-9,8,10,11,11,-9,21,10,18,11,-9
DATA 14,17,15,17,-9,13,19,13,19,-9,16,19,16,19,-9,12,22,12,21,15,21,14,22
DATA 17,21,17,22,-9,6,13,6,17,5,16,5,17,7,17,7,16,-9,-1
kanon:
DATA -2,1,1, 1,0,3,3,5,0,3,3,3,11,2,11,2,9,4,9,4,11,3,11,3,8,0,5
DATA 3,8,6,5,3,8,3,3,-9,11,0,13,2,15,0,16,0,17,1,17,2,15,4,19,8,16,11,8,3,-9
DATA -1
sack:
DATA -4,1,1,1,4,2,5,2,0,3,0,3,8,2,8,4,8,3,8,3,6,4,6,4,0,5,0
DATA 5,6,6,5,6,1,-9,-1
fass:
DATA -4,2,0,6,0,8,3,8,5,6,8,2,8,0,5,0,3,2,0,3,0,3,3,2,4,3,5,3,8
DATA 5,8,5,5,6,4,5,3,5,1,-9,-1
kuge:
DATA -2,1,1, 1,0,0,1,0,3,1,4,3,4,4,3,4,1,3,0,-9,-1
turm:
DATA -4,23,30,27,0,3,10,24,20,6,30,23,30,5,20,26,10,2,0,6,30,-9
DATA 8,0,8,31,9,34,11,36,14,37,15,37,18,36,20,34,21,31,21,29,20,26,18,24,15,23
DATA 14,23,11,24,9,26,8,29,-9,14,23,15,37,-9,10,25,19,35,-9,10,35,19,25,-9
DATA 21,30,21,0,-9,-1
fig:
DATA 0,0,15,20,30,20,20,15,10,0,10,-30,18,-18,20,-5,24,-6,20,-25,10,-40,0,-45
DATA -10,-40,-20,-25,-24,-6,-20,-5,-18,-18,-10,-30,-10,0,-20,15,-30,20,-15,20
DATA -1,-1

' *************************** Audienz beim Koenig ******************************/
sprueche:
DATA "[0][Der König meint:             |'"
DATA "'|                                 ][Demütig zur Kenntnis genommen]"
DATA "Naja...| Nun gut...| Weiter so..."
DATA "Ich bin zufrieden| mit Ihren Leistungen!"
DATA "Hervorragend,| Weiter so!"
DATA "Vielleicht sollten Sie mal| die Steuern senken..."
DATA "Wenn Sie so weiter machen| werde ich Sie entlassen!"
DATA "Vielleicht mal 'nen| Förderturm kaufen..."
DATA "Sie sollten sich| gefälligst mehr Mühe| geben!"
DATA "Sie brauchen nicht| jede Runde zu kommen."
DATA "Wissen Sie eigentlich,| dass Sie mich bereits| xxmal besucht haben?"
DATA "Und Sie sind sich sicher,| daß Sie auch ohne eine| Windfahne zurecht kommen?"
DATA "Schön, Sie zu sehen..."
DATA "Was soll ich denn in| so einer frühen Phase| schon sagen?"
DATA "Sie sollten mehr Geld| verdienen, Fördertürme bauen| und den Gegner besiegen."
DATA "Ich habe Ihnen nichts| neues zu sagen."
DATA "Find' ich nett, daß| Sie mich mal besuchen!"

' Original (german) menu entries
menudata:
DATA "Desk"
DATA "  Ballerburg Info"
DATA "--------------------"
DATA "  Desk Accessory 1  "
DATA "  Desk Accessory 2  "
DATA "  Desk Accessory 3  "
DATA "  Desk Accessory 4  "
DATA "  Desk Accessory 5  "
DATA "  Desk Accessory 6  "
DATA ""
DATA "Spiel"
DATA "  Neues Spiel "
DATA "  Namen der Spieler"
DATA "  Optionen"
DATA "--------------------"
DATA "  Ende"
DATA ""
DATA "Modus"
DATA "  Spieler  - Spieler "
DATA "  Spieler  - Computer"
DATA "  Computer - Spieler  "
DATA "  Computer - Computer"
DATA "----------------------"
DATA "  Computer auswählen"
DATA ""
DATA "Regeln"
DATA "  Spielregeln "
DATA ""
DATA "Ergebnisse"
DATA "  Namen eintragen"
DATA "  Tabelle laden"
DATA "  Tabelle speichern"
DATA "--------------------"
DATA "  Tabelle zeigen"
DATA ""
DATA "***"

' English translateion of the menu entries

menudata_en:
DATA "Desk"
DATA "  Ballerburg Info"
DATA "--------------------"
DATA "  Desk Accessory 1  "
DATA "  Desk Accessory 2  "
DATA "  Desk Accessory 3  "
DATA "  Desk Accessory 4  "
DATA "  Desk Accessory 5  "
DATA "  Desk Accessory 6  "
DATA ""
DATA "Game"
DATA "  New Game "
DATA "  Names of the Players"
DATA "  Options"
DATA "-----------------------"
DATA "  Quit"
DATA ""
DATA "Modes"
DATA "  Player   - Player "
DATA "  Player   - Computer"
DATA "  Computer - Player  "
DATA "  Computer - Computer"
DATA "------------------------"
DATA "  Select Computer (Bot)"
DATA ""
DATA "Rules"
DATA "  show rules ... "
DATA ""
DATA "Results"
DATA "  Enter names ..."
DATA "  Load highscore table "
DATA "  Save highscore table "
DATA "-----------------------"
DATA "  Show highscore table "
DATA ""
DATA "***"
