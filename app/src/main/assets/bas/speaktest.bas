' This demonstrates the use of the SPEAK command
' (currently only useful on Android devices)
'

SPEAK ""  ! the first call to SPEAK will initialize the engine and will speak out nothing
PAUSE 1.5 ! give it some time to initialize

SPEAK "Hello everybody. Are you hungry?"
PAUSE 1
SPEAK "Yes, I am really hungry!",0.5
PAUSE 4

SPEAK "The time is "+TIME$,0.9,1.3
PAUSE 1

SPEAK "Das ganze funktioniert auch auf Deutsch!",-1,-1,"de"
END
