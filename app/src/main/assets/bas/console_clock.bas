'
' ANALOGUHR    V.1.08   console-Version
' Demoprogramm. Es soll nur die Funktionalitaet von X11-BASIC
' demonstrieren. Hier kann man zur Syntax etc. lernen
' Letzte Bearbeitung 09.03.2003  Markus Hoffmann
' Letzte Bearbeitung 18.01.2008  Markus Hoffmann
'
bw=COLS/2
bh=ROWS
bx=0
by=0
CLS
PRINT CHR$(27)+"["+STR$(0)+";"+STR$(30+1)+";"+STR$(40+7)+"m";
PRINT " Analoguhr mit X-BASIC	"
PRINT "     von Markus Hoffmann "
x=bx+bw/2
y=by+bh/2
xr=30
PRINT CHR$(27)+"[m";

'
' ***** Radien fuer Zeiger usw. berechnen.
'

r=MIN(x,y)
bs=r/50
bm=r/20
t=r/12
r1=r
r2=r*0.914
r3=r*0.857
r4=r*0.84
r5=r*0.8
r6=r*0.571
'
' ***** Aktuelle Zeit in Ti$ uebernehmen.
'
@u_hr_zeichnen_1

CLR ti$
al$="Systemzeit oder Neueingabe|der Uhrzeit?"
' ALERT 2,al$,1,"System|Eingabe",v
v=1
IF v=1
  ti$=LEFT$(TIME$,2)
  ti$=ti$+MID$(TIME$,4,2)
  ti$=ti$+RIGHT$(TIME$,2)
ELSE
  CLS
  PRINT AT(10,10);"Bitte geben Sie die ";
  PRINT "aktuelle Uhrzeit ein (HHMMSS): ";
  INPUT ti$
ENDIF
'
'
' ***** Werte aus Ti$ an Uhrvariablen uebergeben.
'
s=VAL(MID$(ti$,5,2))
w1=s-15
m=VAL(MID$(ti$,3,2))
w2=m-15
st=VAL(MID$(ti$,1,2))
IF st>12
  st=st-12
ENDIF
st=st*5+INT(m/12)
w3=st-15
@u_hr_zeichnen_2
'
ti=STIMER
'
' ***** Warten, bis eine Sekunde vergangen ist.
'
DO
  IF STIMER>ti
    ti=STIMER
    ' pbox 120,250,280,300
    @s_ekunde
    PRINT chr$(27)+"["+STR$(0)+";"+STR$(30+5)+";"+STR$(40+9)+"m";
    PRINT at(10,7);time$
    PRINT at(150/16,5);date$
    FLUSH
    PAUSE 1-TIMER+STIMER-0.05
  ENDIF
LOOP
'
'
PROCEDURE s_ekunde
  INC s
  PRINT chr$(27)+"["+STR$(0)+";"+STR$(30+7)+";"+STR$(40+9)+"m";
  @gLINE(x,y,x1,y1)
  PRINT chr$(27)+"["+STR$(0)+";"+STR$(30+4)+";"+STR$(40+9)+"m";
  INC w1
  x1=x+INT(COS(w1*6*PI/180)*r5)
  y1=y+INT(SIN(w1*6*PI/180)*r5)
  @gLINE(x,y,x1,y1)
  IF s=60
    CLR s
    @m_inute
  ENDIF
  PRINT chr$(27)+"["+STR$(0)+";"+STR$(30+3)+";"+STR$(40+9)+"m";
  @gLINE(x,y,x2,y2)
  @gLINE(x,y,x3,y3)
RETURN

PROCEDURE m_inute
  INC m
  PRINT chr$(27)+"["+STR$(0)+";"+STR$(30+7)+";"+STR$(40+9)+"m";
  @gLINE(x,y,x2,y2)
  PRINT chr$(27)+"["+STR$(0)+";"+STR$(30+2)+";"+STR$(40+9)+"m";
  INC w2
  x2=x+INT(COS(w2*6*PI/180)*r5)
  y2=y+INT(SIN(w2*6*PI/180)*r5)
  @gLINE(x,y,x2,y2)
  IF m/12=INT(m/12)
    @s_tunde
  ENDIF
RETURN
PROCEDURE s_tunde
  INC st
  PRINT chr$(27)+"["+STR$(0)+";"+STR$(30+7)+";"+STR$(40+9)+"m";
  @gLINE(x,y,x3,y3)
  PRINT chr$(27)+"["+STR$(0)+";"+STR$(30+2)+";"+STR$(40+9)+"m";
  INC w3
  x3=x+INT(COS(w3*6*PI/180)*r6)
  y3=y+INT(SIN(w3*6*PI/180)*r6)
  @gLINE(x,y,x3,y3)
  IF st=60
    CLR st
  ENDIF
RETURN
PROCEDURE u_hr_zeichnen_1
  w=-15
  REPEAT
    INC w
    x4=x+INT(COS(w*6*PI/180)*r3)
    y4=y+INT(SIN(w*6*PI/180)*r3)
    x5=x+INT(COS(w*6*PI/180)*r2)
    y5=y+INT(SIN(w*6*PI/180)*r2)
    x5a=x+INT(COS(w*6*PI/180)*r4)
    y5a=y+INT(SIN(w*6*PI/180)*r4)
    x6=x+INT(COS(w*6*PI/180)*r1)
    y6=y+INT(SIN(w*6*PI/180)*r1)
    @gLINE(x4,y4,x5,y5)
    IF w/5=INT(w/5)
      @gLINE(x5a,y5a,x5,y5)
      PRINT at(y5a,x5a*2);STR$((-w+5)/5)
    ENDIF
  UNTIL w=45
RETURN
PROCEDURE u_hr_zeichnen_2
  x1=x+INT(COS(w1*6*PI/180)*r5)
  y1=y+INT(SIN(w1*6*PI/180)*r5)
  @gLINE(x,y,x1,y1)
  x2=x+INT(COS(w2*6*PI/180)*r5)
  y2=y+INT(SIN(w2*6*PI/180)*r5)
  @gLINE(x,y,x2,y2)
  x3=x+INT(COS(w3*6*PI/180)*r6)
  y3=y+INT(SIN(w3*6*PI/180)*r6)
  @gLINE(x,y,x3,y3)
RETURN
PROCEDURE plot(x,y)
  PRINT AT(y,x*2);"##";
RETURN
PROCEDURE circle(cx,cy,cr)
  LOCAL i
  FOR i=0 TO 360 STEP 10
    @gline(cx+cr*cos(i/180*pi),cy+cr*sin(i/180*pi),cx+cr*cos((i+10)/180*pi),cy+cr*sin((i+10)/180*pi))
  NEXT i
RETURN
PROCEDURE gline(x1,y1,x2,y2)
  LOCAL dx,dy,row,col,final
  dX=x2-x1
  dY=y2-y1
  pos_slope=abs(dX>0)
  IF dY<0
    pos_slope=1-pos_slope
  ENDIF
  IF ABS(dX)>ABS(dY)
    IF dX>0
      col=x1
      row=y1
      final=x2
    ELSE
      col=x2
      row=y2
      final=x1
    ENDIF
    inc1=2*ABS(dY)
    G=inc1-ABS(dX)
    inc2=2*(ABS(dY)-ABS(dX))
    IF pos_slope
      WHILE col<=final
        @plot(col,row)
        INC col
        IF G>=0
          INC row
          ADD G,inc2
        ELSE
          ADD G,inc1
        ENDIF
      WEND
    ELSE
      WHILE col<=final
        @plot(col,row)
        INC col
        IF G>0
          DEC row
          ADD G,inc2
        ELSE
          ADD G,inc1
        ENDIF
      WEND
    ENDIF
  ELSE
    IF dY>0
      col=x1
      row=y1
      final=y2
    ELSE
      col=x2
      row=y2
      final=y1
    ENDIF
    inc1=2*ABS(dX)
    G=inc1-ABS(dY)
    inc2=2*(ABS(dX)-ABS(dY))
    IF pos_slope
      WHILE row<=final
        @plot(col,row)
        INC row
        IF G>=0
          INC col
          ADD G,inc2
        ELSE
          ADD G,inc1
        ENDIF
      WEND
    ELSE
      WHILE row<=final
        @plot(col,row)
        INC row
        IF G>0
          DEC col
          ADD G,inc2
        ELSE
          ADD G,inc1
        ENDIF
      WEND
    ENDIF
  ENDIF
RETURN
