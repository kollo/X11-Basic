' gfalist.bas fuer Android, linux und WINDOWS
'
' Converts all GFA-BASIC .gfa files (found in the /sdcard/bas/ folder)
' into X11-Basic .bas files.
' (c) Markus Hoffmann 2016
'
' Anleitung f"ur Android:
' das binary gfalist ist in der X11-Basic app enthalten und
' ist im Ordner bas/ abgelegt fuer drei Prozessor-Architekturen.
' Evtl. muessen Sie das anpassen
'
' Comment: converted .gfa file usually need do be manually edited
' before they can be successfully run by X11-Basic.
'

PRINT "initializing..."
FLUSH
prefix$=""
basedir$="."
IF ANDROID?
  sqls$="/usr/bin/gfalist"
  IF NOT EXIST(sqls$)
    ' Try to install it
    sqls$="/data/data/net.sourceforge.x11basic/gfalist"
    IF NOT EXIST(sqls$)
      PRINT "gfalist is not yet installed."
      @android_install
    ENDIF
  ENDIF
  tmp$="/sdcard/gfa.err"
  prefix$="HOME=/sdcard/ "+CHR$(10)
  basedir$="/sdcard/bas"
ELSE IF WIN32?
  sqls$="gfalist"
  tmp$="gfa.err"
ELSE
  sqls$="gfalist"
  tmp$="/tmp/gfa.err"
ENDIF
t$=SYSTEM$(prefix$+sqls$+""+" 2>&1")
IF LEN(t$)=0
  PRINT "something is wrong. gfalist is not responding."
  END
ENDIF
' PRINT "GFALIST Version: "+t$

' Search .gfa and .GFA files
PRINT "search .gfa and .GFA files...."
ON ERROR CONT            ! Skip any error like permission denied or so....
a$=FSFIRST$(basedir$,"*")
WHILE len(a$)
  SPLIT a$," ",0,typ$,name$
  IF typ$="d"                ! Is it a directory?
  ELSE
    IF GLOB(name$,"*.gfa") OR GLOB(name$,"*.GFA")          ! Check if the filename matches the pattern...
      @doone(basedir$+"/"+name$)
    ENDIF
  ENDIF
  ON ERROR CONT          ! Skip any error like permission denied or so....
  a$=FSNEXT$()
WEND
PRINT "done."
IF ANDROID?
  END
ELSE
  QUIT
ENDIF
PROCEDURE doone(infile$)
  LOCAL outfile$
  outfile$=infile$+".bas"
  PRINT "converting "+infile$
  IF not EXIST(outfile$)
    t$=SYSTEM$(prefix$+sqls$+" -o "+outfile$+" "+infile$+" 2>&1")
    PRINT t$
  ELSE
    PRINT "ERROR: "+outfile$+" already exist. Will not overwrite."
  ENDIF
RETURN

PROCEDURE android_install
  LOCAL t$
  PRINT "installing Androids gfalist in "+sqls$
  IF EXIST("/sdcard/bas/gfalist.armeabi")
    PRINT "copying..."
    FLUSH
    OPEN "I",#1,"/sdcard/bas/gfalist.armeabi"
    t$=INPUT$(#1,LOF(#1))
    CLOSE #1
    BSAVE sqls$,VARPTR(t$),LEN(t$)
    ' PRINT system$("cp /mnt/sdcard/bas/sqlite3 "+sqls$+" 2>&1")
    PRINT "change mode..."
    FLUSH
    PRINT SYSTEM$("/system/bin/chmod 755 "+sqls$+" 2>&1")
    FLUSH
  ELSE
    PRINT "cannot find gfalist. please download it "
    PRINT "and put it in /sdcard/bas/"
    END
  ENDIF
RETURN

