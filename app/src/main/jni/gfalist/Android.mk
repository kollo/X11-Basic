LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := gfalist
LOCAL_SRC_FILES := charset.c  gfalist.c  sky.c  tables.c

LOCAL_CFLAGS := -fsigned-char -std=c99 -fPIE
LOCAL_LDFLAGS+= -pie

include $(BUILD_EXECUTABLE)
