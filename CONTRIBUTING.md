Guide to contributing to X11-Basic for Android
==============================================

X11-Basic exists for quite a while now. Therefore I consider it to be in a
quite mature state. I am planning not to extend the language much, e.g. I do
not want to add many more commands or functions. The language as it is should
stay much the same as it is now. 

The language itself as well as the reference implentation for linux have a 
separate git repository. It is called X11Basic and can be found eiter on 
codeberg aswell as on github.

So if you find bugs in the language reference, the manual or the library, please
post your issues there. 

Is it about the Android IDE, Android features like GPS or sensor support, 
user interface etc. This repository here is right.


There are ideas for adding Bluetooth 
support and USB support to the Android version of X11-Basic. This should lead 
to as few additional commands and functions as possible. Maybe it can be done 
usinge external binaries called via the shell (like the SQL support is 
implemented).

Difficult bugs:
===============

There is a strange bug related to screen refresh in the Android version of 
X11-Basic. Does anybody have an idea what could be the problem?

The libgmp library is missing in the F-Droid version of X11-Basic because I have 
not (yet) managed to automatically compile it from sources. Any help here is 
welcomed.


More things left to do:
=======================
- test the thing and find more bugs,
- Optimize a bit more, improve performance,
- documentation needs more work,
- translate the user manual into other languares (german, french,...)
- port it to apple ipad/iphone,
(etc. etc.)

## License and attribution

All contributions must be properly licensed and attributed. If you are
contributing your own original work, then you are offering it under a CC-BY
license (Creative Commons Attribution). If it is code, you are offering it under
the GPL-v2. You are responsible for adding your own name or pseudonym in the
Acknowledgments file, as attribution for your contribution.

If you are sourcing a contribution from somewhere else, it must carry a
compatible license. The project was initially released under the GNU public
licence GPL-v2 which means that contributions must be licensed under open
licenses such as MIT, CC0, CC-BY, etc. You need to indicate the original source
and original license, by including a comment above your contribution. 


## Contributing with a Merge Request

The best way to contribute to this project is by making a merge request:

1. Login with your codeberg account or create one now
2. [Fork](https://codeberg.org/kollo/X11-Basic.git) the X11-Basic for Android repository. 
Work on your fork.
3. Create a new branch on which to make your change, e.g.
`git checkout -b my_code_contribution`, or make the change on the `new` branch.
4. Edit the file where you want to make a change or create a new file in the 
   `contrib` directory if you're not sure where your contribution might fit.
5. Edit `doc/ACKNOWLEGEMENTS` and add your own name to the list of contributors 
   under the section with the current year. Use your name, or a 
   codeberg/github ID, or a pseudonym.
6. Commit your change. Include a commit message describing the correction.
7. Submit a merge/pull request against the X11-Basic repository.



## Contributing with an Issue

If you find a mistake and you're not sure how to fix it, or you don't know how
to do a merge/pull request, then you can file an Issue. Filing an Issue will 
help us see the problem and fix it.

Create a [new Issue](https://codeberg.org/kollo/X11-Basic/issues/new?issue) now!



## Thanks

We are very grateful for your support. With your help, this BASIC implementation
will be a great project. 


So you are welcome to help.
